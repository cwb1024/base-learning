package com.wbcoder.map.threadlocal;

public class MyThreadLocalMap {

    private Object[] table;

    private int size;

    private int threshold;

    public void resize(){
        Object[] oldTab = table;
        int oldLen = oldTab.length;
        int newLen = oldLen * 2;

        Object[] newTab = new Object[newLen];
        int count = 0;

        for (Object e : oldTab) {
            if (e != null) {
                //重置
            } else {
                //开放寻址法
                //计算hash地址
                int h = 0;
                while (newTab[h] != null) {
//                    h = nextIndex(h, newLen);
                }
                newTab[h] = e;
                count++;
            }
        }

//        setThresHold(newLen);
        size = count;
        table = newTab;
    }

    private int nextIndex(int i, int len) {
        return ((i + 1 < len) ? i + 1 : 0);
    }
}
