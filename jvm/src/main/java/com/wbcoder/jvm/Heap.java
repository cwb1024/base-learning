package com.wbcoder.jvm;

import java.util.ArrayList;
import java.util.List;

/**
  * @description: 模拟堆内存无法申请，导致溢出的情况
  * @author: chengwb
  * @Date: 2020-04-05 16:05
  */
public class Heap {

    public static void main(String[] args) {

//        //for 不断的申请空间
//
//        List<Byte[]> list = new ArrayList<>();
//
//        int i = 0;
//        boolean flag = true;
//
//        while (flag) {
//
//            try {
//                i++;
//                list.add(new Byte[1024 * 1024]);
//                Thread.sleep(50);
//            } catch (Exception e) {
//                System.out.println("创建的次数：" + i);
//                e.printStackTrace();
//                flag = false;
//            }
//
//        }

    }
}
