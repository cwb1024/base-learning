package com.wbcoder.jvm;

/**
 * @description: 测试主动调用系统gc方法
 * @author: chengwb
 * @Date: 2020-04-05 18:00
 */
public class ReferenceCountingGc {

    public Object instance = null;

    private static final int _1MB = 1024 * 1024;

    private byte[] bigSize = new byte[2 * _1MB];

    public static void main(String[] args) {

//        ReferenceCountingGc objA = new ReferenceCountingGc();
//        ReferenceCountingGc objB = new ReferenceCountingGc();
//
//        objA.instance = objB;
//        objB.instance = objA;
//
//        objA = null;
//        objB = null;
//
//        //假设这个时候发生GC，验证ObjA 和 ObjB 是否能够回收
//
//        System.gc();
    }
}
