package com.wbcoder.pattern.singleton.check;

import java.io.*;

public class Singleton_serialAble implements Serializable {

    private Singleton_serialAble() { }

    private static class SingletonHolder {
        private static final Singleton_serialAble instance = new Singleton_serialAble();
    }

    public static Singleton_serialAble getInstance() {
        return SingletonHolder.instance;
    }

    //该方法是反序列化调用函数，用于对反序列化的数据时代替反序列化出的对象
    private Object readResolve() throws ObjectStreamException {
        return getInstance();
    }

    private static final String outputPath = "/Users/chengwenbi/code/base-learning/singleton/"+ Singleton_serialAble.class.getName()+".db";

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        checkSerializeCrack();
    }

    //做序列化｜反序列化验证
    private static void checkSerializeCrack() throws IOException, ClassNotFoundException{

        //构建内存对象
        Singleton_serialAble instance = new Singleton_serialAble();
        //序列化
        FileOutputStream fos = new FileOutputStream(outputPath);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(instance);

        //反序列化
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(outputPath));
        Singleton_serialAble tmp = (Singleton_serialAble) ois.readObject();

        ois.close();
        oos.close();
        fos.close();

        System.out.println("序列化与反序列化与常规获取内存地址是否一致："+(tmp == Singleton_serialAble.getInstance()));

        //是否是同一个对象地址：true
    }
}
