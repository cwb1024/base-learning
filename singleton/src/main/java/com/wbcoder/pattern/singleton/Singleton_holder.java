package com.wbcoder.pattern.singleton;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class Singleton_holder implements Serializable{

    private Singleton_holder() {}

    // 静态内部类-线程安全，利用类只加载一次特性
    private static class InstanceHolder {
        private final static Singleton_holder instance = new Singleton_holder();
    }

    public static Singleton_holder getInstance() {
        return InstanceHolder.instance;
    }

    // 非常规获取对象验证
    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException,IOException, ClassNotFoundException {
        //反射获取
        checkReflectionCrack();
        //序列化
        checkSerializeCrack();
    }

    //做反射爆破验证,通过反射的方式可以拿到这个类的别的实例对象。
    private static void checkReflectionCrack() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {

        //直接拿一个对象
        Singleton_holder instance = Singleton_holder.getInstance();

        //反射拿对象
        Class<?> classType = Singleton_holder.class;
        Constructor<?> c = classType.getDeclaredConstructor(null);
        c.setAccessible(true);
        Singleton_holder singleton_holder = (Singleton_holder) c.newInstance();

        System.out.println(String.format("反射爆破与常规获取内存地址是否一致: " + (singleton_holder == instance)));

        //反射爆破与常规获取内存地址是否一致: false
    }

    private static final String outputPath = "/Users/chengwenbi/code/base-learning/singleton/"+ Singleton_holder.class.getName()+".db";
    //做序列化｜反序列化验证
    private static void checkSerializeCrack() throws IOException, ClassNotFoundException{

        //构建内存对象
        Singleton_holder instance = new Singleton_holder();
        //序列化
        FileOutputStream fos = new FileOutputStream(outputPath);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(instance);

        //反序列化
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(outputPath));
        Singleton_holder tmp = (Singleton_holder) ois.readObject();

        System.out.println("序列化与反序列化与常规获取内存地址是否一致："+(tmp == Singleton_holder.getInstance()));

        //序列化与常规获取内存地址是否一致：false
    }
}
