package com.wbcoder.pattern.singleton;

//threadLocal避开了DCL的问题，但是却增大了内存开销.
//因为threadLocal本质上是用一个hashmap来管理的这些变量，键为线程对象，值为该线程对应的局部变量副本的值。
public class Singleton_threadLocal {

    private Singleton_threadLocal(){}

    private static Singleton_threadLocal singleton;

    private static ThreadLocal<Singleton_threadLocal> threadLocal = new ThreadLocal<>();

    public static Singleton_threadLocal getInstance() {
        if (threadLocal.get()==null) { //a
            synchronized (Singleton_threadLocal.class) { //b
                //获取实例之前检查是否为空
                if (singleton == null) { //c
                    //第一次获取的时候总是为空的
                    //初始化实例
                    singleton = new Singleton_threadLocal(); //d
                }
            }
            threadLocal.set(singleton); //e
        }
        return singleton; //f
    }

}
