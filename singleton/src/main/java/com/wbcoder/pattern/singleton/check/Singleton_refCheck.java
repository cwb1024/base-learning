package com.wbcoder.pattern.singleton.check;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

// 防止反射暴力破解，构造方法显式检测解决--自定义报错check
public class Singleton_refCheck {

    private static boolean isNew = false;

    //私有构造方法做check，抛出异常不允许再次创建
    private Singleton_refCheck() {
        synchronized (Singleton_refCheck.class) {
            if (!isNew) {
                isNew = true;
            } else {
                //这种方式会在程序内提示报错信息，合理否，不优雅！！！
                throw new RuntimeException("单例模式被破坏.");
            }
        }
    }

    //下面可以使用原来的几种单例实现方式
    //比如：这里使用静态内部类的形式
    private static class InstanceHolder {
        private final static Singleton_refCheck instance = new Singleton_refCheck();
    }

    public static Singleton_refCheck getInstance() {
        return InstanceHolder.instance;
    }

    //非常规获取对象验证
    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        checkReflectionCrack();
    }

    //做反射验证：反射在通过newInstance创建对象时，会检查该类是否ENUM修饰，如果是则抛出异常，反射失败。
    private static void checkReflectionCrack() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {

        //直接拿一个对象
        Singleton_refCheck e2 = Singleton_refCheck.getInstance();

        //反射创建
        Class<?> classType = Singleton_refCheck.class;
        Constructor<?> c = classType.getDeclaredConstructor(null);
        c.setAccessible(true);
        Singleton_refCheck e1 = (Singleton_refCheck) c.newInstance();

        assert e1 == e2;

        //Caused by: java.lang.RuntimeException: 单例模式被破坏.
        //	at com.wbcoder.singleton.check.Singleton_07.<init>(Singleton_07.java:18)
        //	... 6 more
    }

}
