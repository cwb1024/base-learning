package com.wbcoder.pattern.singleton;

public class Singleton_synchronized {

    private Singleton_synchronized() {}

    private static Singleton_synchronized singleton;

    //方式一： synchronized 防止多线程创建
    public synchronized static Singleton_synchronized getInstance() {
        if (singleton == null) {
            singleton = new Singleton_synchronized();
        }
        return singleton;
    }

    //方式二：锁住代码块，缩小锁的范围
    public static Singleton_synchronized getInstanceElse() {
        synchronized (Singleton_synchronized.class) {
            if (singleton == null) {
                singleton = new Singleton_synchronized();
            }
        }
        return singleton;
    }
}
