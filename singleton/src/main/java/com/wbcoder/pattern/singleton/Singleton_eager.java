package com.wbcoder.pattern.singleton;

public class Singleton_eager {

    private Singleton_eager(){}

    //预加载-线程安全
    private static Singleton_eager singleton = new Singleton_eager();

    public static Singleton_eager getInstance(){
        return singleton;
    }

}
