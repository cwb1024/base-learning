package com.wbcoder.pattern.singleton;

import java.util.concurrent.atomic.AtomicReference;

public class Singleton_cas_loop {
    
    private Singleton_cas_loop(){}

    private static final AtomicReference<Singleton_cas_loop> INSTANCE = new AtomicReference<>();

    public static Singleton_cas_loop getInstance(){
        for (;;){
            Singleton_cas_loop singleton = INSTANCE.get();
            if (null != singleton){
                return singleton;
            }

            singleton = new Singleton_cas_loop();
            if (INSTANCE.compareAndSet(null,singleton)){
                return singleton;
            }
        }
    }
}
