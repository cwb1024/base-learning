package com.wbcoder.pattern.singleton;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

//注册表
public class SingletonRegistry {

    //key:类的全限定名，value：类对象
    private final static Map<String, Object> singletonObjects = new ConcurrentHashMap<>();

    //预初始化
    static {
        SingletonRegistry singletonRegistry = new SingletonRegistry();
        singletonObjects.put(SingletonRegistry.class.getName(), singletonRegistry);
    }

    private SingletonRegistry() {}

    public static SingletonRegistry getInstance(String name) {
        if (name != null) {
            if (singletonObjects.get(name) == null) {
                try {
                    singletonObjects.put(name, Class.forName(name).newInstance());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            return (SingletonRegistry) singletonObjects.get(name);
        }
        return null;
    }

    // 非常规获取对象验证
    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, IOException, ClassNotFoundException {
        //反射获取
        checkReflectionCrack();
        //序列化
        checkSerializeCrack();
        //使用
        usage_registerBean("com.example.demo.singleton.SingletonRegistry");
    }

    //使用方式
    private static void usage_registerBean(String fullyQualifiedName) {

    }

    //做反射爆破验证,通过反射的方式可以拿到这个类的别的实例对象。
    private static void checkReflectionCrack() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {

        //直接拿一个对象
        SingletonRegistry instance = SingletonRegistry.getInstance("com.example.demo.singleton.SingletonRegistry");

        //反射拿对象
        Class<?> classType = SingletonRegistry.class;
        Constructor<?> c = classType.getDeclaredConstructor(null);
        c.setAccessible(true);
        SingletonRegistry singletonRegistry = (SingletonRegistry) c.newInstance();

        System.out.println(String.format("反射爆破与常规获取内存地址是否一致: " + (singletonRegistry == instance)));

        //反射爆破与常规获取内存地址是否一致: false
    }

    private static final String outputPath = "/Users/chengwenbi/code/base-learning/singleton/"+ SingletonRegistry.class.getName()+".db";
    //做序列化｜反序列化验证
    private static void checkSerializeCrack() throws IOException, ClassNotFoundException{

        //构建内存对象
        SingletonRegistry instance = SingletonRegistry.getInstance("com.example.demo.singleton.SingletonRegistry");
        //序列化
        FileOutputStream fos = new FileOutputStream(outputPath);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(instance);

        //反序列化
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(outputPath));
        Singleton_holder tmp = (Singleton_holder) ois.readObject();

        System.out.println("序列化与反序列化与常规获取内存地址是否一致："+(tmp == Singleton_holder.getInstance()));

        //序列化与常规获取内存地址是否一致：false
    }
}
