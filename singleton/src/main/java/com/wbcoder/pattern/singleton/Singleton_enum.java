package com.wbcoder.pattern.singleton;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

// 利用jdk枚举类内置隐式实现，可以避免反射暴力爆破问题-->反射报错
public enum Singleton_enum {

    INSTANCE;

    public static Singleton_enum getInstance() {
        return INSTANCE;
    }

    //非常规获取对象验证
    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        checkReflectionCrack();
    }

    //做反射验证：反射在通过newInstance创建对象时，会检查该类是否ENUM修饰，如果是则抛出异常，反射失败。
    private static void checkReflectionCrack() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {

        //直接拿一个对象
        Singleton_enum instance = Singleton_enum.getInstance();

        //通过反射拿到一个实例对象
        Class<?> classType  = Singleton_enum.class;
        Constructor<?> constructor = classType.getDeclaredConstructor(null);
        constructor.setAccessible(true);
        Singleton_enum singleton06 = (Singleton_enum) constructor.newInstance();

        System.out.println(String.format("反射爆破与常规获取内存地址是否一致: " + (singleton06 == instance)));

        //Exception in thread "main" java.lang.NoSuchMethodException: com.wbcoder.singleton.Singleton_06.<init>()
        //	at java.lang.Class.getConstructor0(Class.java:3082)
        //	at java.lang.Class.getDeclaredConstructor(Class.java:2178)
        //	at com.wbcoder.singleton.Singleton_06.checkPrint(Singleton_06.java:21)
        //	at com.wbcoder.singleton.Singleton_06.main(Singleton_06.java:13)
    }
}
