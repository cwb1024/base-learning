package com.wbcoder.pattern.singleton;

public class Singleton_lazy {

    private Singleton_lazy(){}

    private static Singleton_lazy singleton;

    //懒加载-线程不安全
    public static Singleton_lazy getInstance(){
        if (singleton == null){
            singleton = new Singleton_lazy();
        }
        return singleton;
    }
}
