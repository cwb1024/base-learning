package com.wbcoder.pattern.singleton;

public class Singleton_dcl {

    private Singleton_dcl() {}

    private static volatile Singleton_dcl singleton;

    //DCL
    public static Singleton_dcl getInstance() {
        // check1
        if (null == singleton) {
            synchronized (Singleton_dcl.class) {
                // check2
                if (null == singleton) {
                    singleton = new Singleton_dcl();
                }
            }
        }
        return singleton;
    }
}
