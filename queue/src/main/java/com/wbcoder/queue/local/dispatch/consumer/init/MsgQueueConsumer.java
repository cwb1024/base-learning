package com.wbcoder.queue.local.dispatch.consumer.init;

import com.wbcoder.queue.local.dispatch.consumer.task.PollingLocalMsgTask;

import java.util.concurrent.ExecutorService;

public class MsgQueueConsumer {

    //初始化 利用 InitializingBean 也可以
    static {
        ExecutorService executorService = ThreadPool.getInstance();
        executorService.submit(new PollingLocalMsgTask());
    }

    public static void main(String[] args) {

    }
}
