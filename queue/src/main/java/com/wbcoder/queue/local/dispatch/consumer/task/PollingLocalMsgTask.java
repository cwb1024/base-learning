package com.wbcoder.queue.local.dispatch.consumer.task;

import com.wbcoder.queue.local.LocalBusinessQueue;

import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class PollingLocalMsgTask implements Runnable{

    /**
     * 整体提交的阈值
     */
    private static final int cntPerTime = 50;

    /**
     * 临时buffer
     */
    private List<LocalBusinessQueue.Message> messageBuffer = new ArrayList<>();
    private Map<Integer, Integer> messagePvMap = new HashMap<>();
    private Map<Integer, Integer> messageUvMap = new HashMap<>();

    @Override
    public void run() {

        while (true){

            BlockingQueue<LocalBusinessQueue.Message> queue = LocalBusinessQueue.getInstance();

            try {

                LocalBusinessQueue.Message message = queue.poll(1, TimeUnit.SECONDS);

                if (Objects.nonNull(message)) {
                    messageBuffer.add(message);
                }

                //如果队列为空
                if (messageBuffer.size() >= cntPerTime || (queue.isEmpty() && !messageBuffer.isEmpty())) {

                    populateMessage(messageBuffer);
                    messageBuffer.clear();

                    accBatchPv();
                    accBatchUv();

                    messageUvMap.clear();
                    messagePvMap.clear();
                } else {
                    // cpu 闲置5s
                    Thread.sleep(5000);
                    continue;
                }


            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    private void populateMessage(List<LocalBusinessQueue.Message> messageBuffer) {
        for (LocalBusinessQueue.Message msg : messageBuffer) {
            String flag = msg.getFlag();
            if ("PV".equals(flag)) {
                Integer cnt = messagePvMap.get(msg.getBusinessUniqueId());
                if (cnt == null) cnt = 1;
                else cnt++;
                messagePvMap.put(msg.getBusinessUniqueId(), cnt);
            } else if ("UV".equals(flag)) {
                Integer cnt = messageUvMap.get(msg.getBusinessUniqueId());
                if (cnt == null) cnt = 1;
                else cnt++;
                messageUvMap.put(msg.getBusinessUniqueId(), cnt);
            }
        }
    }

    private void accBatchPv() {}

    private void accBatchUv() {}

}
