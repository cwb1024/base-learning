package com.wbcoder.queue.local;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * 本地业务队列
 */
public class LocalBusinessQueue {

    //做单例化
    private LocalBusinessQueue() {}

    private volatile static BlockingQueue<Message> queue;

    //DCL
    public static BlockingQueue<Message> getInstance() {
        if (queue == null) {
            synchronized (LocalBusinessQueue.class) {
                if (queue == null) {
                    queue = new LinkedBlockingQueue<>();
                } else {
                    return queue;
                }
            }
        }
        return queue;
    }


    //消息体
    public static class Message {
        private Integer businessUniqueId;
        private String flag;

        public Integer getBusinessUniqueId() {
            return businessUniqueId;
        }

        public void setBusinessUniqueId(Integer businessUniqueId) {
            this.businessUniqueId = businessUniqueId;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }

        public String getFlag() {
            return flag;
        }
    }
}
