package com.wbcoder.queue.local.dispatch.consumer.init;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPool {

    private ThreadPool(){}

    private static ExecutorService executor = Executors.newFixedThreadPool(1);

    public static ExecutorService getInstance() {
        return executor;
    }
}
