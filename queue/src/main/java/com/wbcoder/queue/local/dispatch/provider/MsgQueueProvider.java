package com.wbcoder.queue.local.dispatch.provider;

import com.wbcoder.queue.local.LocalBusinessQueue;

import java.util.concurrent.BlockingQueue;

//消息入队
public class MsgQueueProvider {

    private static BlockingQueue<LocalBusinessQueue.Message> businessQueue;

    static {
        businessQueue = LocalBusinessQueue.getInstance();
    }

    //build message
    public static LocalBusinessQueue.Message buildMessage(String topic,Integer uniqueId){
        LocalBusinessQueue.Message message = new LocalBusinessQueue.Message();

        message.setFlag(topic);
        message.setBusinessUniqueId(uniqueId);
        return message;
    }

    //offer
    private static void send(){
        businessQueue.offer(buildMessage("myTopic",001));
    }

    public static void main(String[] args) {
        //send message,集成到请求主线程
        MsgQueueProvider.send();
    }
}
