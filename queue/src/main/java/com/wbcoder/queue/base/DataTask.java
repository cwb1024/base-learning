package com.wbcoder.queue.base;

import java.io.*;
import java.util.List;

public class DataTask implements Runnable {

    private List<String> taskData;
    private String rootPath = "/Users/chengwenbi/tmpfile/";

    public DataTask(List<String> taskData) {
        this.taskData = taskData;
    }

    @Override
    public void run() {
        //1、拿到数据
        if (taskData == null) {
            return;
        }
        FileOutputStream fos = null;
        try {
            File file = new File(rootPath + Thread.currentThread().getName() + ".txt");
            if (file == null) {
                file.createNewFile();
            }
            fos = new FileOutputStream(file);
            String NEW_LINE = System.getProperty("line.separator");
            for (String id : taskData) {
                //todo 耗时的任务 处理一次要 1s
                Thread.sleep(1000);
                System.out.println("threadName ==> " + Thread.currentThread().getName() + " ; id==>" + id);
                //进行文件的按行写入
                fos.write(id.getBytes());
                fos.write(NEW_LINE.getBytes());
            }
            fos.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
