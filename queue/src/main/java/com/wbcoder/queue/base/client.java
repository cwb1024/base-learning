package com.wbcoder.queue.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description: 数据一次性的加载到内存，共享数据进行分组，派发给各个线程来执行
 * @author: chengwb
 * @Date: 2019-12-21 00:30
 */
public class client {

    public static List<String> getData() {
        List<String> idlist = new ArrayList<>();
        for (int i = 0; i < 9999; i++) {
            idlist.add(i + "行啊。");
        }
        return idlist;
    }

    public static void main(String[] args) throws InterruptedException {
        long startTime = System.nanoTime();
        //数据分割，分给10个人来搞这件事，数据分割均匀，简单的取模分割
        List<String> data = getData();
        if (data == null) {
            return;
        }
        int mod = 10;
        Map<Integer, List<String>> integerListMap = new HashMap<>();
        //取模分配
        for (int j = 0; j < data.size(); j++) {
            int key = j % mod;
            List<String> strings = integerListMap.get(key);
            if (strings == null) {
                strings = new ArrayList<>();
            }
            strings.add(data.get(j));
            integerListMap.put(key, strings);
        }
        //根据分组情况，开启多个线程，来搞这件事
        for (Map.Entry<Integer, List<String>> entry : integerListMap.entrySet()) {
            Integer key = entry.getKey();
            List<String> value = entry.getValue();
            Thread thread = new Thread(new DataTask(value));
            thread.setName(key + "");
            thread.start();
        }
        long endTime = System.nanoTime();
        System.out.println("耗时"+(endTime-startTime)+"ms");
    }
}
