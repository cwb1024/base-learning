package com.wbcoder.queue.mq.base;

import java.util.concurrent.ArrayBlockingQueue;

/**
  * @description: 这里搞一个多线程从队列里边消费
  * @author: chengwb
  * @Date: 2019-12-21 01:36
  */
public class Consumer {

    public static void main(String[] args) {

        //从队列里边获取消息，并且取出消息，进行消费处理
        ArrayBlockingQueue blockingDeque = MessageQueue.getInstance().blockingDeque;
        System.out.println(blockingDeque.size());
        //这里死循环，一直等待数据的到来，一直取数据
        while (true) {
            try {
                Object take = blockingDeque.take();
                System.out.println(take);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
