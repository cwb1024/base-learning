package com.wbcoder.queue.mq.asyn;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * @description:
 * @author: chengwb
 * @Date: 2019-12-21 02:11
 */
public class Client {


    public static void main(String[] args) {
        //利用jdk 队列，数据加载到内存，进行多线程的生产，多线程的消费，队列里的数据是共享数据
        //搞一个队列,比如这里的队列长度就是2，后面可以调优啊
        ArrayBlockingQueue arrayBlockingQueue = new ArrayBlockingQueue(1000);
        //这里搞一个线程池，设置一个池的核心线程内容，当数据没有发完的时候，就从线程池内获取
        Thread producer = new Thread(new Producer(arrayBlockingQueue));
        Thread consumer = new Thread(new Consumer(arrayBlockingQueue));
        producer.start();
        consumer.start();
    }
}
