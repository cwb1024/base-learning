package com.wbcoder.queue.mq.asyn;

import java.util.ArrayList;
import java.util.List;

public class DataSource {

    static List<String> idList;
    static {
        List<String> idList = new ArrayList<>();
        for (int i = 0; i < 9999; i++) {
            idList.add(i + "data item.");
        }
    }

    private DataSource(){}

    private static DataSource dataSource = new DataSource();

    public static DataSource getInstance(){
        return dataSource;
    }

    public static List<String> getIdList() {
        return idList;
    }
}
