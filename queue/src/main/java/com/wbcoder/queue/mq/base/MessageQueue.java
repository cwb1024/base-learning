package com.wbcoder.queue.mq.base;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * @description: 利用JDK的阻塞队列来搞一个共享数据的地方，用于生产者往里边放，消费者从里边取
 * 这里搞一个单例的类，就弄一个单例的队列，全局使用这个队列
 * @author: chengwb
 * @Date: 2019-12-21 01:29
 */
public class MessageQueue{

    private MessageQueue(){}
    private static MessageQueue messageQueue = new MessageQueue();

    public static ArrayBlockingQueue blockingDeque = new ArrayBlockingQueue(20000);

    public static MessageQueue getInstance() {
        return messageQueue;
    }
}
