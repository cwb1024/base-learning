package com.wbcoder.queue.mq.asyn;


import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * @description: 作用是往队列里生产消息
 * @author: chengwb
 * @Date: 2019-12-21 02:15
 */
public class Producer implements Runnable {

    private ArrayBlockingQueue arrayBlockingQueue;

    public Producer(ArrayBlockingQueue arrayBlockingQueue) {
        this.arrayBlockingQueue = arrayBlockingQueue;
    }

    @Override
    public void run() {
        List<String> idList = DataSource.getInstance().getIdList();
        for (String i : idList) {
            try {
                Thread.sleep(200);
                arrayBlockingQueue.put(i);
                System.out.println("生产者产品了产品" + i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
