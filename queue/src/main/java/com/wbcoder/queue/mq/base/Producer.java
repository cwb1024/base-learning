package com.wbcoder.queue.mq.base;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

/**
  * @description: 客户端，准备数据，并且放到共享的阻塞队列内，准备搞一个多线程往队列里边发送消息
  * @author: chengwb
  * @Date: 2019-12-21 01:27
  */
public class Producer {

    public static List<String> genData(){
        List<String> idlist = new ArrayList<>();
        for (int i = 0; i < 9999; i++) {
            idlist.add(i + "行啊。");
        }
        return idlist;
    }

    public static void main(String[] args) {

        //拿到这个队列
        ArrayBlockingQueue blockingDeque = MessageQueue.getInstance().blockingDeque;
        //获取数据
        List<String> data = genData();
        //往队列发送消息，如果队列满了，就等着
        for (int i = 0; i < data.size(); i++) {
            try {
                blockingDeque.put(data.get(i));
                System.out.println("开始放数据"+i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
