package com.wbcoder.queue.mq.asyn;


import java.util.concurrent.ArrayBlockingQueue;

/**
  * @description: 从队列里消费消息
  * @author: chengwb
  * @Date: 2019-12-21 02:15
  */
public class Consumer implements Runnable {

    private ArrayBlockingQueue arrayBlockingQueue ;

    public Consumer(ArrayBlockingQueue arrayBlockingQueue) {
        this.arrayBlockingQueue = arrayBlockingQueue;
    }

    @Override
    public void run() {
        //找到队列
        try {
            while (true){
                Object take = arrayBlockingQueue.take();
                System.out.println("消费者消费了产品：==> " + take);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
