package com.wbcoder.regex;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainClient {

    public static void main(String[] args) {
//        Pattern p = Pattern.compile("(\\d{3,4})\\-(\\d{7,8})");
//        Matcher m = p.matcher("010-12345678");
//        if (m.matches()) {
//            String g0 = m.group(0);
//            String g1 = m.group(1);
//            String g2 = m.group(2);
//            System.out.println(g0);
//            System.out.println(g1);
//            System.out.println(g2);
//        } else {
//            System.out.println("匹配失败!");
//        }
//
//        testMethod();
//        testMethod2(null);
//        testMethod3(null);
//        testMethod4(null);
        testMethod5(null);
//        testMethod6(null);

    }

    public static void testMethod(){

        String regex = "(\\d{3,4})\\-(\\d{7,8})";

        String s = "010-12345678";


        System.out.println(s.matches(regex));
    }

    public static void testMethod2(String[] args) {
        Pattern pattern = Pattern.compile("(\\d+)(0*)");
        Matcher matcher = pattern.matcher("1230000");
        if (matcher.matches()) {
            System.out.println("group1=" + matcher.group(1)); // "1230000"
            System.out.println("group2=" + matcher.group(2)); // ""
        }
    }

    public static void testMethod3(String[] args) {
        Pattern pattern = Pattern.compile("(\\d+?)(0*)");
        Matcher matcher = pattern.matcher("1230000");
        if (matcher.matches()) {
            System.out.println("group1=" + matcher.group(1)); // "123"
            System.out.println("group2=" + matcher.group(2)); // "0000"
        }
    }


    public static void testMethod4(String[] args) {
        String s = "the quick brown fox jumps over the lazy dog.";
        Pattern p = Pattern.compile("\\wo\\w");
        Matcher m = p.matcher(s);
        while (m.find()) {
            String sub = s.substring(m.start(), m.end());
            System.out.println(sub);
        }
    }

    /**
     * 特殊情况有 * ^ : | . \，当我们使用这些特殊字符的时候，如果没有注意，就有可能会得到一些偏离我们预期的结果
     * @param args
     */
    public static void testMethod5(String[] args) {
        String s = "the .quick .brown .fox .jumps .over .the .lazy .dog.";
        //字符串替换
        s = s.replace(".", "\\");
        //正则分割
        String[] split = s.split("\\\\");
        System.out.println(Arrays.toString(split));
    }


    public static void testMethod6(String[] args) {

        String s = "acount=? and uu =? or n=?";
        //正则分割，多个字符分割
        String[] split = s.split("and|or");
        System.out.println(Arrays.toString(split));

    }

}


