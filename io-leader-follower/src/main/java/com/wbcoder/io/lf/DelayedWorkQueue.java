package com.wbcoder.io.lf;

import java.util.concurrent.RunnableScheduledFuture;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import static java.util.concurrent.TimeUnit.NANOSECONDS;

/**
 * JDK 延迟队列 leader-follower 线程优化
 */

/**
 * IO线程模型一直在演化，
 * 由最开始的单线程模型，
 * 到BIO方式的单线程接受请求线程池线程具体处理单个请求的读写事件，
 * 再到NIO的单线程接受请求线程池里面的单个线程可以处理不同请求的读写事件，一个字没有最快，只有更快。
 * 最近发现还有个Leader-follower线程模型，其的出现是为了解决单线程接受请求线程池线程处理请求下线程上下文切换以及线程间通信数据拷贝的开销，
 * 并且不需要维护一个队列。
 *
 *
 *
 * 在Leader-follower线程模型中每个线程有三种模式，leader,follower, processing。
 * 在Leader-follower线程模型一开始会创建一个线程池，并且会选取一个线程作为leader线程，leader线程负责监听网络请求，其它线程为follower处于waiting状态，当leader线程接受到一个请求后，会释放自己作为leader的权利，然后从follower线程中选择一个线程进行激活，然后激活的线程被选择为新的leader线程作为服务监听，然后老的leader则负责处理自己接受到的请求（现在老的leader线程状态变为了processing），处理完成后，状态从processing转换为。follower
 * 可知这种模式下接受请求和进行处理使用的是同一个线程，这避免了线程上下文切换和线程通讯数据拷贝。
 *
 *
 * （1）线程有3种状态：领导leading，处理processing，追随following
 *
 * （2）假设共N个线程，其中只有1个leading线程（等待任务），x个processing线程（处理），余下有N-1-x个following线程（空闲）
 *
 * （3）有一把锁，谁抢到就是leading
 *
 * （4）事件/任务来到时，leading线程会对其进行处理，从而转化为processing状态，处理完成之后，又转变为following
 *
 * （5）丢失leading后，following会尝试抢锁，抢到则变为leading，否则保持following
 *
 * （6）following不干事，就是抢锁，力图成为leading
 *
 * 优点：不需要消息队列
 *
 * 适用场景：线程能够很快的完成工作任务
 *
 */


public class DelayedWorkQueue {

    private final ReentrantLock lock = new ReentrantLock();
    private final Condition available = lock.newCondition();
    private RunnableScheduledFuture<?>[] queue = null;
    private Thread leader = null;

    /**
     * 从堆顶拉数据
     *
     * @param nanos
     * @return
     * @throws InterruptedException
     */
    public RunnableScheduledFuture<?> poll(long nanos) throws InterruptedException {
        for (; ; ) {
            //自旋从堆顶拉数据
            RunnableScheduledFuture<?> first = queue[0];
            //没有数据
            if (first == null) {
                //本次允许的拉取时间结束了
                if (nanos <= 0)
                    return null;
                else
                    //等待着，返回剩余的拉取时间
                    nanos = available.awaitNanos(nanos);
            } else {
                //如果堆顶有数据
                long delay = first.getDelay(NANOSECONDS);
                //这个节点的延迟结束了，允许执行取出
                if (delay <= 0)
                    //取出并且调整
                    return finishPoll(first);
                //有限时间到了，不返回
                if (nanos <= 0)
                    return null;
                first = null; // don't retain ref while waiting

                //多个线程都可以来抢这个堆顶数据
                //如果有限时间小于延迟时间，拉不出来，等待有限时间，期望有别的操作可以打断这个等待
                //如果leader不为空，leader 开始做等待拿着个数据
                if (nanos < delay || leader != null)
                    nanos = available.awaitNanos(nanos);
                else {
                    //多个follower线程可以来抢leader位了
                    Thread thisThread = Thread.currentThread();
                    leader = thisThread;
                    try {
                        //做延迟耗时等待，应该拉到了
                        long timeLeft = available.awaitNanos(delay);
                        //循环耗时重置
                        nanos -= delay - timeLeft;
                    } finally {
                        //leader处理了delay时间，处理完了，进入follower过程
                        if (leader == thisThread)
                            leader = null;
                    }
                }
            }
        }
    }

    private RunnableScheduledFuture<?> finishPoll(RunnableScheduledFuture<?> first) {
        //siftDown
        return first;
    }
}
