package com.wbcoder.spi.impl;


import com.wbcoder.spi.inter.IConvertStrategy;

public class PNGConvertStrategy implements IConvertStrategy {
    @Override
    public void convert(Object o) {
        System.out.println(this.getClass().getName() + " is running...");
    }
}
