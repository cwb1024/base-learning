package com.wbcoder.spi.inter;

public interface IConvertStrategy<T> {

    void convert(T t);
}
