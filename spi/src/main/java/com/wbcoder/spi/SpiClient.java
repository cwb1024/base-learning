package com.wbcoder.spi;

import com.wbcoder.spi.inter.IConvertStrategy;

import java.util.ServiceLoader;

public class SpiClient {

    public static void main(String[] args) {

        ServiceLoader<IConvertStrategy> loaders = ServiceLoader.load(IConvertStrategy.class);

        for (IConvertStrategy strategy: loaders) {
            strategy.convert(null);
        }

    }
}
