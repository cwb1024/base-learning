package com.wbcoder.encode;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Base64;

public class EncodeClient {


    /**
     * URL编码是编码算法，不是加密算法。URL编码的目的是把任意文本数据编码为%前缀表示的文本，
     * 编码后的文本仅包含A~Z，a~z，0~9，-，_，.，*和%，便于浏览器和服务器处理。
     */
    public static void urlEncodeOrDecode() {
        String encoded = null;
        try {
            encoded = URLEncoder.encode("中文!", "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        System.out.println(encoded);

        try {
            String decode = URLDecoder.decode(encoded, "utf-8");
            System.out.println(decode);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }


    /**
     * Base64编码的目的是把二进制数据变成文本格式，这样在很多文本中就可以处理二进制数据。例如，电子邮件协议就是文本协议，如果要在电子邮件中添加一个二进制文件，就可以用Base64编码，然后以文本的形式传送。
     *
     * Base64编码的缺点是传输效率会降低，因为它把原始数据的长度增加了1/3。
     *
     * 和URL编码一样，Base64编码是一种编码算法，不是加密算法。
     *
     * 如果把Base64的64个字符编码表换成32个、48个或者58个，就可以使用Base32编码，Base48编码和Base58编码。字符越少，编码的效率就会越低。
     */
    public static void base64Byte2Text() {
        byte[] input = new byte[]{(byte) 0xe4, (byte) 0xb8, (byte) 0xad};
        String b64encoded = Base64.getEncoder().encodeToString(input);
        System.out.println(b64encoded);
    }


    public static void main(String[] args) {
        urlEncodeOrDecode();

        base64Byte2Text();
    }

}
