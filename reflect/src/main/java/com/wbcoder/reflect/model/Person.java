package com.wbcoder.reflect.model;

/**
 * @description: 神奇的用于测试反射的类啊
 * @author: chengwb
 * @Date: 2019-11-07 22:42
 */
public class Person {

    /**
     * 名称
     */
    public String name;

    /**
     * 年龄
     */
    public Integer age;
    /**
     * IdCard
     */
    private String idCard;

    /**
     * 薪水
     */
    double salary;

    public Person() {
    }

    public Person(String name, Integer age, String idCard, double salary) {
        this.name = name;
        this.age = age;
        this.idCard = idCard;
        this.salary = salary;
    }

    public void work() {
        System.out.println("person is working...");
    }

    private void learnAlone() {
        System.out.println("person is learning ...");
    }

    public void buyHouse(double price) {
        System.out.println("you can get one house which value of " + price);
    }

    /**
     * 晋升
     */
    public void promotion() {
        work();
        learnAlone();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
