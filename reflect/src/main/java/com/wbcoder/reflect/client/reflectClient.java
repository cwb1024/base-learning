package com.wbcoder.reflect.client;

import com.wbcoder.reflect.model.Animal;
import com.wbcoder.reflect.model.Person;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;

/**
 * @description: 模拟程序运行期间的反射调用情况
 * @author: chengwb
 * @Date: 2019-11-07 22:51
 */
public class reflectClient {

    //1、全类名加载class文件
    public static Class getPersonClass1() throws Exception {
        return Class.forName("com.wbcoder.reflect.model.Person");
    }

    //2、通过类名获取反射对象
    public static Class getPersonClass2() throws Exception {
        return Person.class;
    }

    //3、通过类的实例对象获取反射对象,多个实例对象获取到的类对象都是一个
    public static Class getPersonClass3() throws Exception {
        Person person1 = new Person();
        return person1.getClass();
    }

    /**
     * 反射的引入方式
     */
    public static void reflectInit() throws Exception {
        //1、全类名加载class文件
        Class p1 = getPersonClass1();
        //2、通过类名获取反射对象
        Class p2 = getPersonClass2();
        //3、通过类的实例对象获取反射对象,多个实例对象获取到的类对象都是一个
        Class p3 = getPersonClass3();
        Class p31 = getPersonClass3();
        System.out.println(p1 == p2);
        System.out.println(p1 == p3);
        System.out.println(p2 == p3);
        System.out.println(p3 == p31);
    }

    /**
     * 属性
     * @throws Exception
     */
    public static void reflectFields() throws Exception {
        Class person = getPersonClass1();
        System.out.println("Fields:||||||||||||||||||||⬇️||||||||||||||||||||");
        System.out.println("成员变量：" + person.getFields());
        System.out.println("Fields:||||||||||||||||||||⬆️||||||||||||||||||||");
        System.out.println();
        System.out.println("declaredFields:||||||||||||||||||||⬇️||||||||||||||||||||");
        System.out.println("declared成员变量" + person.getDeclaredFields());
        System.out.println("declaredFields:||||||||||||||||||||⬆️||||||||||||||||||||");
        System.out.println();
        //指定参数值
        Field[] fields = person.getFields();
        for (Field field : fields) {
            System.out.println(field.getType());
        }
        //得到参数的基本类型后，怎么指定一些值上去

        //复杂的，组合方式的反射
    }

    /**
     * 构造器
     * @throws Exception
     */
    public static void reflectConstructors() throws Exception {
        Class person = getPersonClass1();
        System.out.println("Constructors:||||||||||||||||||||⬇️||||||||||||||||||||");
        Constructor[] constructors = person.getConstructors();
        for (Constructor constructor : constructors) {
            System.out.println("构造器：" + constructor);
            System.out.println(constructor.getParameters());
        }
        System.out.println("Constructors:||||||||||||||||||||⬆️||||||||||||||||||||");
        System.out.println();
        System.out.println("declaredConstructors:||||||||||||||||||||⬇️||||||||||||||||||||");
        Constructor[] declaredConstructors = person.getDeclaredConstructors();
        for (Constructor constructor : declaredConstructors) {
            System.out.println("declared构造器：" + constructor);
            System.out.println(constructor.getParameters());
        }
        System.out.println("declaredConstructors:||||||||||||||||||||⬆️||||||||||||||||||||");
        System.out.println();
        //指定参数值


        //反射获取构造器，创建类对象
        Object o = constructors[0].newInstance();
    }

    /**
     * 方法
     *  场景：为了通用，方法的参数应该是知道的，但是对于方法的重载调用呢？
     *  要调用这个方法，这个方法的参数我可能不知道吗？不知道这个方法的执行结果该怎么确定？
     *  那我可以获取参数的类型吗？
     *  可以传入参数，传入指定的然后赋值吗？
     * 【1】、获取到指定方法，怎么传参数，调用invoke方法，使得方法执行？
     *
     *  invoke 的过程发生了什么？
     *
     * @throws Exception
     */
    public static void reflectMethods() throws Exception {
        Class person = getPersonClass1();
        System.out.println("Methods:||||||||||||||||||||⬇️||||||||||||||||||||");
        Method[] methods = person.getMethods();
        for (Method method : methods) {
            System.out.println("⬇️方法：" + method);
            System.out.println("⬆️方法属性：" + method.getName());
            //方法怎么执行呢？

            //类对象怎么产生一个实例
//            Person pp = new Person();
//            method.invoke(pp);
        }
        System.out.println("Methods:||||||||||||||||||||⬆️||||||||||||||||||||");
        System.out.println();
        System.out.println("declaredMethods:||||||||||||||||||||⬇️||||||||||||||||||||");
        Method[] declaredMethods = person.getDeclaredMethods();
        for (Method method : declaredMethods) {
            System.out.println("⬇️declared方法：" + method);
            System.out.println("⬆️declared方法属性：" + method.getName());
        }
        System.out.println("declaredMethods:||||||||||||||||||||⬆️||||||||||||||||||||");
        System.out.println();

        //指定方法,这里的输入如果不写死，可以怎么搞
        Method work = person.getMethod("work");
        //申明对象，搞一个实例，调用方法
        Person ppp = new Person();
        work.invoke(ppp);

        //通过类对象的newInstance创建一个对象，进行方法的调用
        work.invoke(person.newInstance());


        Class<?>[] parameterTypes = null;
        for (Method mth : person.getMethods()) {
            if (mth.getName().equals("buyHouse")) {
                parameterTypes = mth.getParameterTypes();
                System.out.println(parameterTypes);
                break;
            }
        }
        Method buyHouse = person.getMethod("buyHouse",parameterTypes);
        //可能我不知道，我这个方法有没有参数啊
        buyHouse.invoke(person.newInstance(),3000000);
    }

    /**
     * 注解
     * @throws Exception
     */
    public static void reflectAnnotations() throws Exception {
        Class person = getPersonClass1();
        System.out.println("Annotations:||||||||||||||||||||⬇️||||||||||||||||||||");
        Annotation[] annotations = person.getAnnotations();
        for (Annotation annotation : annotations) {
            System.out.println("注解" + annotation);
            System.out.println("注解属性" + annotation.annotationType());
        }
        System.out.println("Annotations:||||||||||||||||||||⬆️||||||||||||||||||||");
        System.out.println();
        System.out.println("Annotations:||||||||||||||||||||⬇️||||||||||||||||||||");
        Annotation[] declaredAnnotations = person.getDeclaredAnnotations();
        for (Annotation annotation : declaredAnnotations) {
            System.out.println("注解" + annotation);
            System.out.println("注解属性" + annotation.annotationType());
        }
        System.out.println("Annotations:||||||||||||||||||||⬆️||||||||||||||||||||");
        System.out.println();
        //指定注解
    }

    /**
     * 接口
     * @throws Exception
     */
    public static void reflectInterfaces() throws Exception {
        Class person = getPersonClass1();
        System.out.println("Interfaces:||||||||||||||||||||⬇️||||||||||||||||||||");
        Class[] interfaces = person.getInterfaces();
        for (Class supInterface : interfaces) {
            System.out.println("interface:" + supInterface);
        }
        System.out.println("Interfaces:||||||||||||||||||||⬆️||||||||||||||||||||");
        System.out.println();
        System.out.println("annotatedInterfaces:||||||||||||||||||||⬇️||||||||||||||||||||");
        AnnotatedType[] annotatedInterfaces = person.getAnnotatedInterfaces();
        for (AnnotatedType annotatedType : annotatedInterfaces) {
            System.out.println("注解接口" + annotatedType);
        }
        System.out.println("annotatedInterfaces:||||||||||||||||||||⬆️||||||||||||||||||||");
        System.out.println();
        System.out.println("genericInterfaces:||||||||||||||||||||⬇️||||||||||||||||||||");
        Type[] genericInterfaces = person.getGenericInterfaces();
        for (Type type : genericInterfaces) {
            System.out.println("类型接口" + type);
        }
        System.out.println("genericInterfaces:||||||||||||||||||||⬆️||||||||||||||||||||");
        System.out.println();
    }

    /**
     * 父类
     * @throws Exception
     */
    public static void reflectSupClass() throws Exception {
        Class person = getPersonClass1();
        System.out.println("SuppClass:||||||||||||||||||||⬇️||||||||||||||||||||");
        Class superclass = person.getSuperclass();
        System.out.println("父类" + superclass);
        System.out.println("SuppClass:||||||||||||||||||||⬆️||||||||||||||||||||");
    }

    /**
     * 字类
     * @throws Exception
     */
    public static void reflectSubClass() throws Exception {
        Class person = getPersonClass1();
//        person.asSubclass(Animal.class);
    }

    /**
     * 内部类：普通内部类、普通内部类+静态、函数内的内部类、匿名内部类
     */
    public static void reflectInnerClass()throws Exception{

    }



    /**
     * 反射对单例模式的爆破 ！！！！！爆破爆破爆破爆破爆破！！！
     * 创建出来了多个对象，
     * 需要对单例类改造
     * @param
     * @throws Exception
     */
    public static void reflectNewInstance()throws Exception{
        Class<Animal> animalClass = Animal.class;
        Animal animal1 = animalClass.newInstance();
        Animal animal2 = animalClass.newInstance();
        Animal animal3 = animalClass.newInstance();

        System.out.println(animal1 == animal2);
        System.out.println(animal1 == animal3);
        System.out.println(animal3 == animal2);
    }

    /**
     * 反射对单例的爆破，解决方案！！！！！
     * @throws Exception
     */
    public static void reflectSingleton()throws Exception{

    }




    public static void main(String[] args) throws Exception {
        /**
         * 申明方式
         */
//        reflectInit();
        /**
         * API的使用
         */
//        reflectFields();
//        reflectConstructors();
//        reflectMethods();
//        reflectAnnotations();
//        reflectInterfaces();
//        reflectSupClass();
//        reflectSubClass();
        /**
         * 多个类间的调用
         */

        reflectNewInstance();
    }
}
