package com.wbcoder.io.concurrent.model.nio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;

public class NIOServer {

    public static void main(String[] args) {

        try {


            Selector selector = Selector.open();

            ServerSocketChannel serverSocket = ServerSocketChannel.open();
            serverSocket.bind(new InetSocketAddress(9000));
            serverSocket.configureBlocking(false);
            serverSocket.register(selector, SelectionKey.OP_CONNECT);


        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
