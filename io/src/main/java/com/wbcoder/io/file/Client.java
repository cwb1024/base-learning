package com.wbcoder.io.file;

import java.io.IOException;

/**
 * @description: 测试文件IO的拷贝操作
 * @author: chengwb
 * @Date: 2020/5/5 19:50
 */
public class Client {

    static String fromPath = "/Users/chengwenbi/tmpfile/source.txt";
    static String toPath = "/Users/chengwenbi/tmpfile/source1.txt";

    public static void main(String[] args) throws IOException {

//        FileUtil.copyFile(fromPath, toPath);

//        FileUtil.copyFileByChannel_ByteBuf(fromPath, toPath);

        FileUtil.randomAccessFileCopy(fromPath, toPath);
    }
}
