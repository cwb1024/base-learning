package com.wbcoder.io.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * 文件生成MD5
 */
public class FileMD5 {

    protected static char[] hexDigits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    protected static MessageDigest messagedigest = null;

    static {
        try {
            messagedigest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    /**
     * 方法的参数是接收一个文件File对象，并且返回该文件生成的"MD5"值。
     *
     * @param file
     * @return
     * @throws IOException
     */
    public static String getFileMD5String(File file) throws IOException {
        FileInputStream fileInS = new FileInputStream(file);
        byte[] buffer = new byte[1024];
        int numRead = 0;
        while ((numRead = fileInS.read(buffer)) > 0) {
            messagedigest.update(buffer, 0, numRead);
        }
        fileInS.close();
        return bufferToHex(messagedigest.digest());
    }


    /**
     * @param inputStream
     * @return
     */
    public static String streamToMD5(InputStream inputStream) {
        try {
            MessageDigest mdTemp = MessageDigest.getInstance("MD5");
            byte[] buffer = new byte[1024];
            int numRead = 0;
            while ((numRead = inputStream.read(buffer)) > 0) {
                mdTemp.update(buffer, 0, numRead);
            }
            return bufferToHex(mdTemp.digest()).toUpperCase();
        } catch (NoSuchAlgorithmException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 这里定义了一个转化方法，
     * 把文件或文件流以字节的方式进行转换，
     * 使之生成唯一的md5，
     * 此方法间接的应用到了md5的生成中。
     *
     * @param bytes
     * @return
     */
    private static String bufferToHex(byte[] bytes) {
        return bufferToHex(bytes, 0, bytes.length);
    }

    private static String bufferToHex(byte[] bytes, int m, int n) {
        StringBuffer stringBuffer = new StringBuffer(2 * n);
        int k = m + n;
        for (int l = m; l < k; ++l) {
            appendHexPair(bytes[l], stringBuffer);
        }
        return stringBuffer.toString();
    }

    /**
     * 根据提供的文件流输入流InputStream进行生成md5的值，
     * 如上几个函数中的代码加在一起就是完整的文件或文件流的md5值生成方法。
     *
     * @param bt
     * @param stringbuffer
     */
    private static void appendHexPair(byte bt, StringBuffer stringbuffer) {
        char c0 = hexDigits[((bt & 0xF0) >> 4)];
        char c1 = hexDigits[(bt & 0xF)];
        stringbuffer.append(c0);
        stringbuffer.append(c1);
    }

    public static void main(String[] args) throws Exception{
        String path = "C:\\Users\\L14\\Desktop\\uptest\\xxx.pptx";

        String s = streamToMD5(new FileInputStream(path));

        System.out.println(s);
    }
}
