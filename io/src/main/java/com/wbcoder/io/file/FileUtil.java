package com.wbcoder.io.file;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.List;

/**
 * @description: 利用jdk api
 * 实现对单文件、文件夹的创建、删除、查询
 * 实现文件的拷贝
 * 实现文件夹拷贝
 * @author: chengwb
 * @Date: 2019-09-23 01:20
 */
public class FileUtil {

    /**
     * 创建一个文件
     *
     * @param path
     */
    public static boolean createFile(String path) throws IOException {
        File file = new File(path);
        // 如果文件不存在，创建一个新的文件
        if (!file.exists()) {
            try {
                return file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    /**
     * 删除文件或目录及目录下所有文件
     *
     * @param file
     * @return
     * @throws IOException
     */
    public static boolean deleteFile(File file) throws IOException {
        if (!file.exists()) {
            return false;
        }
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File f : files) {
                deleteFile(f);
            }
        }
        return file.delete();
    }

    /**
     * 创建单级文件夹
     *
     * @param path
     * @throws IOException
     */
    public static boolean createDirectory(String path) throws IOException {
        File file = new File(path);
        return file.mkdir();
    }

    public static boolean createDirectory(File file) throws IOException {
        return file.mkdir();
    }

    /**
     * 创建多级嵌套文件夹
     *
     * @param path
     * @throws IOException
     */
    public static boolean createDirectorys(String path) throws IOException {
        File file = new File(path);
        return file.mkdirs();
    }

    /**
     * 删除文件夹及文件夹下的内容
     *
     * @param path
     * @throws IOException
     */
    public static boolean deleteDirectory(String path) throws IOException {
        return deleteFile(new File(path));
    }

    /**
     * 遍历当前文件夹,只遍历到当前目录下，没有递归的层次遍历,返回的是 dirName
     *
     * @param path
     * @throws IOException
     */
    public static void iterateDirName(String path, List<String> result) throws IOException {
        File file = new File(path);
        String[] list = file.list();
        for (String s : list) {
            result.add(s);
        }
    }

    /**
     * 遍历文件夹
     * 遍历当前文件夹下的全部文件，包括文件夹、文件，没有去把文件中的文件全部遍历出来 遍历出来是文件的绝对路径
     *
     * @param path
     * @throws IOException
     */
    public static void iterateDirAbsolutePath(String path, List<File> result) throws IOException {
        File file = new File(path);
        File[] files = file.listFiles();
        for (File tmpFile : files) {
            result.add(tmpFile);
        }
    }

    /**
     * 遍历指定路径下的所有文件
     *
     * @param path
     * @throws IOException
     */
    public static void iterateAllFiles(String path) throws IOException {
        File file = new File(path);
        File[] filesArray = file.listFiles();
        if (filesArray != null && filesArray.length > 0) {
            for (File tmpFile : filesArray) {
                //递归遍历所有的文件
                if (tmpFile.isDirectory()) {
                    iterateAllFiles(tmpFile.getAbsolutePath());
                } else {
                    System.out.println(tmpFile.getName());
                }
            }
        }
    }

    /**
     * 遍历路径下所有的文件及文件夹
     *
     * @param fileNames    名称结果集
     * @param absolutePath 绝对路径
     * @throws IOException
     */
    public static void iterateAllFilesAndDirs(String absolutePath, List<String> fileNames) throws IOException {
        File file = new File(absolutePath);
        if (!file.exists()) return;
        if (file.isDirectory()) {
            final File[] files = file.listFiles();
            if (files == null || files.length == 0) return;
            for (File f : files) {
                if (f.isDirectory()) {
                    iterateAllFilesAndDirs(f.getAbsolutePath(), fileNames);
                } else {
                    fileNames.add(f.getName());
                }
            }
        } else {
            fileNames.add(file.getName());
        }
    }


    /**
     * 拷贝文件
     * 从sourcePath中拿到数据，读到内存中，在从内存中输出到destPath中
     *
     * @param sourcePath
     * @param destPath
     */
    public static void copyFile(String sourcePath, String destPath) throws IOException {
        try (FileInputStream fis = new FileInputStream(sourcePath);
             FileOutputStream fos = new FileOutputStream(destPath)) {
            int readLen = 0;
            while ((readLen = fis.read()) != -1) {
                //读一个传一个
                fos.write(readLen);
            }
        }
    }

    public static void copyFile(File sourceFile, File destFile) throws IOException {
        try (FileInputStream fis = new FileInputStream(sourceFile);
             FileOutputStream fos = new FileOutputStream(destFile)) {
            int readLen = 0;
            while ((readLen = fis.read()) != -1) {
                //读一个传一个
                fos.write(readLen);
            }
        }
    }


    /**
     * 拷贝所有文件
     * 从sourcePath中拿到数据，读到内存中，在从内存中输出到destPath中
     *
     * @param sourcePath
     * @param destPath
     * @throws IOException
     */
    public static void copyFile1(String sourcePath, String destPath) throws IOException {
        try (FileInputStream fis = new FileInputStream(sourcePath);
             FileOutputStream fos = new FileOutputStream(destPath)) {
            byte[] bs = new byte[1024 * 10];
            int len = 0;
            while ((len = fis.read(bs)) != -1) {
                // 读一点拷贝一点
                fos.write(bs, 0, len);
            }
        }
    }

    /**
     * 拷贝所有文件
     *
     * @param sourcePath
     * @param destPath
     * @throws IOException
     */
    public static void copyFile1(File sourcePath, File destPath) throws IOException {
        try (FileInputStream fis = new FileInputStream(sourcePath);
             FileOutputStream fos = new FileOutputStream(destPath)) {
            byte[] bs = new byte[1024 * 10];
            int len = 0;
            while ((len = fis.read(bs)) != -1) {
                // 读一点拷贝一点
                fos.write(bs, 0, len);
            }
        }
    }

    /**
     * 平滑流速拷贝
     *
     * @param sourcePath
     * @param destPath
     * @throws IOException
     */
    public static void bufferedCopyFile(String sourcePath, String destPath) throws IOException {
        try (FileInputStream fis = new FileInputStream(sourcePath);
             FileOutputStream fos = new FileOutputStream(destPath);
             BufferedInputStream bis = new BufferedInputStream(fis);
             BufferedOutputStream bos = new BufferedOutputStream(fos)) {
            int b;
            //直接写入关联的复制对象文件中
            while ((b = bis.read()) != -1) {
                bos.write(b);
            }
        }
    }

    /**
     * 拷贝文件夹
     *
     * @param sourceDir
     * @param destDir
     * @throws IOException
     */
    public static void copyDirectory(File sourceDir, File destDir) throws IOException {
        // 判断路径是否合法
        if (!sourceDir.exists()) {
            throw new RuntimeException("您输入的文件源不是合法路径！");
        }
        if (!destDir.exists()) {
            destDir.mkdirs();
        }
        // 获取目的源下面的所有文件和文件夹列表
        File[] srcFiles = sourceDir.listFiles();
        for (File srcFile : srcFiles) {
            File dstFile = new File(destDir, srcFile.getName());
            if (srcFile.isDirectory()) {
                copyDirectory(srcFile, dstFile);
            } else {
                copyFile(srcFile, dstFile);
            }
        }
    }

    /**
     * 通过文件通道实现文件的拷贝
     *
     * @throws IOException
     */
    public static void copyFileByChannel_ByteBuf(String sourcePath, String destPath) throws IOException {
        try (FileInputStream fis = new FileInputStream(sourcePath);
             FileOutputStream fos = new FileOutputStream(destPath);
             FileChannel fiCh = fis.getChannel();
             FileChannel foCh = fos.getChannel()) {

            //有了通道，找一个buffer
            ByteBuffer buf = ByteBuffer.allocate(1024);
            while (fiCh.read(buf) != -1) {
                buf.flip();
                foCh.write(buf);
                buf.clear();
            }
        }
    }


    /**
     * @throws IOException
     */
    public static void randomAccessFileCopy(String sourcePath, String destPath) throws IOException {
        try (RandomAccessFile in = new RandomAccessFile(sourcePath, "r");
             RandomAccessFile out = new RandomAccessFile(destPath, "rw");
             FileChannel fiCh = in.getChannel();
             FileChannel foCh = out.getChannel()) {
            long size = fiCh.size();

            //输入流缓冲区
            MappedByteBuffer inMapByteBuf = fiCh.map(FileChannel.MapMode.READ_ONLY, 0, size);
            //输出流缓冲区
            MappedByteBuffer outMapByteBuf = foCh.map(FileChannel.MapMode.READ_WRITE, 0, size);

            for (int i = 0; i < size; i++) {
                outMapByteBuf.put(inMapByteBuf.get());
            }
        }
    }
}
