package com.wbcoder.io.file;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * 临时文件操作
 */
public class TmpFileUtil {

    /**
     * 创建临时文件
     */
    public void createTempFile(String prefix, String suffix) throws Exception {
        File.createTempFile(prefix, suffix);
    }

    /**
     * 创建临时目录
     */
    public void createTempFileDirectory(String prefix, String suffix) throws Exception {
        File directory = new File("tmp_directory");
        File.createTempFile(prefix, suffix, directory);
    }

    /**
     * 临时文件删除
     */
    public void fileDelete(){
        File file = new File("pathname");
        file.delete();
    }

    /**
     * 通过Files工具类操作
     */
    public void createTempFileByFiles(String prefix, String suffix) throws Exception {

        //Files.createTempFile()
    }

}
