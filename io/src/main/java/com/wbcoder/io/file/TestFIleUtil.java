package com.wbcoder.io.file;

import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class TestFIleUtil {


    private String basePathName = "/Users/chengwenbi/code/base-learning/tmpfile";

    private String fromPath = basePathName + "/a/a_a/a_a_info.txt";

    private String toPath = basePathName + "/a/a_a/a_a_info_tmp.txt";

    private String fromPathDir = basePathName + "/a";

    private String toPathDir = basePathName + "/b";

    @Test
    public void testCreateFile() throws IOException {
        FileUtil.createFile(basePathName + File.separator + "a_info1.txt");
    }

    @Test
    public void testDeleteFile() throws IOException {
        FileUtil.deleteFile(new File(basePathName + File.separator + "a_info1.txt"));
    }

    @Test
    public void testCreateDir() throws IOException {
        FileUtil.createDirectory(basePathName + File.separator + "q");
    }

    @Test
    public void testCreateDirs() throws IOException {
        FileUtil.createDirectorys(basePathName + File.separator + "c/d/e");
    }

    @Test
    public void testDeleteDir() throws IOException {

    }

    @Test
    public void testDeleteDirs() throws IOException {

    }

    @Test
    public void testIterateDirName() throws IOException {
        ArrayList<String> result = new ArrayList<>();
        FileUtil.iterateDirName(basePathName, result);
        System.out.println(result);
    }

    @Test
    public void testIterateDirAbsolutePath() throws IOException {
        ArrayList<File> result = new ArrayList<>();
        FileUtil.iterateDirAbsolutePath(basePathName, result);
        System.out.println(result);
    }

    @Test
    public void testIterateAllFiles() throws IOException {
        FileUtil.iterateAllFiles(basePathName);
    }

    @Test
    public void testIterateAllFilesAndDirs() throws IOException {
        FileUtil.iterateAllFilesAndDirs(basePathName, new ArrayList<String>());
    }

    @Test
    public void testCopyFile() throws IOException {
        FileUtil.copyFile(fromPath, toPath);
    }

    @Test
    public void testCopyFiles() throws IOException {
        FileUtil.copyFile1(fromPathDir, toPathDir);
    }

}
