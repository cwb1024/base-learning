package com.wbcoder.io.converter;


import java.io.*;

/**
 * 输入流转换为输出流
 * 输出流转换为输入流
 * 输入流转换为字符串
 * 输出流转换为字符串
 * 字符串转inputStream
 * 字符串转outStream
 * 字节流转换为字符流
 * 字符流转换为字节流
 *
  * @description: IO 流的一些转换,需要读取到内存中进行一次转换
  * @author: chengwb
  * @Date: 2019-09-22 18:58
  */
public class IOConverterUtil {

    /**
     * FileInputStream:访问磁盘，读入内存
     */

    /**
     * FileOutputStream:读出内存，写入磁盘，
     */

    /**
     * FilterInputStream:装饰器设计模式，对InputStream功能增强，对节点类进行封装，实现特殊功能
     */

    /**
     * FilterOutputStream:装饰器设计模式，对OutputStream功能增强
     */

    /**
     * BufferedInputStream:带有缓冲buf功能，平滑流速，衔接不同流速
     */

    /**
     * BufferedOutputStream:带有缓冲buf功能，平滑流速，衔接不同流速
     */

    /**
     * DataInputStream:数据输入流，它是用来装饰其他输入流，作用是"允许应用程序以机器无关方式从底层输入流中读取基本Java数据类型"
     */

    /**
     * DataOutputStream:
     */

    /**
     * ByteArrayInputStream:字节数组输入流，从字节数组(byte[])中进行以字节为单位的读取，也就是将资源文件都以字节的形式存入到该类的字节数组中去
     */

    /**
     * ByteArrayOutputStream
     */

    /**
     * ObjectInputStream:对象输入流，用来提供对基本数据或对象的持计存储。通俗点说，也就是能直接传输对象，通常应用在反序列化中。它也是一种处理流，构造器参数是InputStream
     */

    /**
     * ObjectOutputStream
     */

    /**
     * PrintStream:打印流
     */


    /**
     * 输入流转换为输出流
     */
    public static ByteArrayOutputStream parse(InputStream in)throws IOException {
        //返回结果
        ByteArrayOutputStream swapStream = new ByteArrayOutputStream();
        //读取输入流，写入到输出流内，跟文件拷贝原理一致，这个过程涉及到对内存的占用
        int ch;
        while ((ch = in.read()) != -1) {
            swapStream.write(ch);
        }
        return swapStream;
    }

    /**
     * 输出流转换为输入流
     */
    public static ByteArrayInputStream parse(OutputStream out)throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        //
        baos = (ByteArrayOutputStream) out;
        ByteArrayInputStream swapStream = new ByteArrayInputStream(baos.toByteArray());
        return swapStream;
    }

    /**
     * 输入流转换为字符串
     */
    public static String parseToString(InputStream in) throws IOException{
        ByteArrayOutputStream swapStream = new ByteArrayOutputStream();
        int ch;
        while ((ch = in.read()) != -1) {
            swapStream.write(ch);
        }
        return swapStream.toString();
    }

    /**
     * 输出流转换为字符串
     */
    public static String parseToString(OutputStream out) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        baos = (ByteArrayOutputStream) out;
        ByteArrayInputStream swapStream = new ByteArrayInputStream(baos.toByteArray());
        return swapStream.toString();
    }

    /**
     * 字符串转inputStream
     */
    public static ByteArrayInputStream parseToInputStream(String text)throws IOException {
        ByteArrayInputStream swapStream = new ByteArrayInputStream(text.getBytes());
        return swapStream;
    }

    /**
     * 字符串转outStream
     */
    public static ByteArrayOutputStream parseToOutStream(String text)throws IOException {

        return parse(parseToInputStream(text));
    }

    /**
     * 缓冲流，使用了装饰器模式来实现，主要实现了缓冲的功能，降低了流速快慢不一定的情况,对磁盘的读写一般意义不大
     */
    public static InputStream bufferedIn(InputStream in){
        BufferedInputStream bis = new BufferedInputStream(in);
        return bis;
    }

    /**
     * 缓冲流，使用了装饰器模式来实现，主要实现了缓冲的功能，降低了流速快慢不一定的情况，对磁盘的读写一般意义不大
     * @param out
     * @return
     */
    public static OutputStream bufferedOut(OutputStream out){
        BufferedOutputStream bos = new BufferedOutputStream(out);
        return bos;
    }

    /**
     * 字节流转换为字符流 ======> InputStreamReader
     */


    /**
     * 字符流转换为字节流 =======> OutputStreamWriter
     */



}
