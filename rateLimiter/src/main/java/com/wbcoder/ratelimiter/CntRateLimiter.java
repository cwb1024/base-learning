package com.wbcoder.ratelimiter;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 计数器限流:
 * 具有 "突刺"+临界点double流量的问题
 */
public class CntRateLimiter {

    /**
     * 每秒请求计数器
     */
    private AtomicInteger permitsPerSecond = new AtomicInteger(0);

    /**
     * 限流开始时间
     */
    private long lastTime = System.currentTimeMillis();

    /**
     * 限流上限
     */
    private final int limit = 50;

    /**
     * 时间窗口
     */
    private long lastInterval = 1000;

    /**
     * 时间间隔内满了没有，如果满了，就拒绝都拒绝
     * 时间间隔到了进行重置操作
     * @return
     */
    public boolean acquire() {
        long now = System.currentTimeMillis();
        //时间范围内，进行上限次数的判断
        if (now < lastTime + lastInterval) {
            int cnt = permitsPerSecond.incrementAndGet();
            return cnt <= limit;
        } else {
            //时间间隔到了，进行重置操作
            lastTime = now;
            permitsPerSecond.set(1);
            return true;
        }
    }
}
