package com.wbcoder.cglib.lazy;

import com.wbcoder.cglib.BusinessClass;
import net.sf.cglib.proxy.Dispatcher;

public class ConcreteClassDispatcher implements Dispatcher {
    @Override
    public Object loadObject() throws Exception {
        System.out.println("before Dispatcher...");
        PropertyBean propertyBean = new PropertyBean();
        propertyBean.setKey("xxx");
        propertyBean.setValue(new BusinessClass());
        System.out.println("after Dispatcher...");
        return propertyBean;
    }
}
