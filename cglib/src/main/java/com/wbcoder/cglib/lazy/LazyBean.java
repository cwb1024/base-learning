package com.wbcoder.cglib.lazy;

import net.sf.cglib.proxy.Enhancer;

/**
 * @description: 延迟加载Bean
 * @author: chengwb
 * @Date: 2019-11-10 16:35
 */
public class LazyBean {

    private String name;
    private String age;
    private String id;
    private PropertyBean propertyBean;
    private PropertyBean propertyBeanDispathcher;

    public LazyBean(String name, String age, String id) {
        System.out.println("bean lazy init ...");
        this.name = name;
        this.age = age;
        this.id = id;
        this.propertyBean = createPropertyBean();
        this.propertyBeanDispathcher = createPropertyBeanDispatcher();
    }


    /**
     * 只第一次懒加载
     *
     * @return
     */
    private PropertyBean createPropertyBean() {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(PropertyBean.class);
        PropertyBean pb = (PropertyBean) enhancer.create(PropertyBean.class, new ConcreteClassLazyLoader());

        return pb;
    }

    /**
     * 每次都懒加载
     * @return
     */
    private PropertyBean createPropertyBeanDispatcher() {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(PropertyBean.class);
        PropertyBean pb = (PropertyBean) enhancer.create(PropertyBean.class, new ConcreteClassDispatcher());
        return pb;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


}
