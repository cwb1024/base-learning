package com.wbcoder.cglib.lazy;

import com.wbcoder.cglib.BusinessClass;
import net.sf.cglib.proxy.LazyLoader;

public class ConcreteClassLazyLoader implements LazyLoader {
    @Override
    public Object loadObject() throws Exception {
        System.out.println("before lazyLoader...");
        PropertyBean propertyBean = new PropertyBean();
        propertyBean.setKey("zghw");
        propertyBean.setValue(new BusinessClass());
        System.out.println("after lazyLoader...");
        return propertyBean;
    }
}
