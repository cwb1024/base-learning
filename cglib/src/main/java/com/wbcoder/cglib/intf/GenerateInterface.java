package com.wbcoder.cglib.intf;

import com.wbcoder.cglib.BusinessClass;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.InterfaceMaker;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
  * @description: 据说能生成一个接口啊
  * @author: chengwb
  * @Date: 2019-11-10 16:54
  */
public class GenerateInterface {

    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        InterfaceMaker interfaceMaker = new InterfaceMaker();

        //抽取某个类的方法生成接口方法
        interfaceMaker.add(BusinessClass.class);

        Class targetInterface = interfaceMaker.create();

        for (Method method : targetInterface.getMethods()) {

            System.out.println("接口方法："+method.getName());
        }

        //接口代理并设置代理接口方法拦截
        Object object = Enhancer.create(Object.class, new Class[]{targetInterface}, new MethodInterceptor() {
            @Override
            public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
                if (method.getName().equals("operate1")) {
                    System.out.println("op1");
                    return "op111111";
                }
                if (method.getName().equals("operate2")) {
                    System.out.println("op2");
                    return "op222222";
                }
                if (method.getName().equals("operate3")) {
                    System.out.println("op3");
                    return "op333333";
                }

                return "default";
            }

        });

        Method operate1 = object.getClass().getMethod("operate1", new Class[]{});

        operate1.invoke(object, null);
    }
}
