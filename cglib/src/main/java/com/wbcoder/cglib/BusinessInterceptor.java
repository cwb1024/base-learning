package com.wbcoder.cglib;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @description: 定义一个拦截器啊，拦截到这个方法，可以做一些操作哟
 * @author: chengwb
 * @Date: 2019-11-10 16:05
 */
public class BusinessInterceptor implements MethodInterceptor {


    /**
     * @param o 这是需要代理的对象，也称为目标对象啊
     * @param method 这是需要代理的方法，也称为目标方法啊
     * @param objects 这是方法的参数数组啊
     * @param methodProxy CGLib的代理对象啊
     * @return
     * @throws Throwable
     */
    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        System.out.println("这是方法调用的前边，这里是增强呀...");

        Object result = methodProxy.invokeSuper(o, objects);

        System.out.println("这是方法调用的后边,这里是后置增强呀...");

        return result;
    }
}
