package com.wbcoder.cglib;

import net.sf.cglib.proxy.CallbackFilter;

import java.lang.reflect.Method;

/**
 * @description: 搞一个回调过滤器啊
 * <p>
 * 在CGLib回调时可以设置对不同方法执行不同的回调逻辑，或者根本不执行回调。
 * @author: chengwb
 * @Date: 2019-11-10 16:20
 */
public class BusinessClassCallBackFilter implements CallbackFilter {

    /**
     * 过滤方法
     * 返回的值为数字，代表了Callback数组中的索引位置，要到用的Callback
     */
    @Override
    public int accept(Method method) {
        if (method.getName().equals("operate1")) {
            return 0;
        }

        if (method.getName().equals("operate2")) {
            return 1;
        }

        if (method.getName().equals("operate3")) {
            return 2;
        }

        return 0;
    }
}
