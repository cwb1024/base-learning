package com.wbcoder.cglib;

import net.sf.cglib.proxy.Callback;
import net.sf.cglib.proxy.CallbackFilter;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.NoOp;

/**
  * @description: 我们来生成一个代理类啊
  * @author: chengwb
  * @Date: 2019-11-10 16:13
  */
public class GenerateProxyClass {

    public static void main(String[] args) {

//        Enhancer类是CGLib中的一个字节码增强器，它可以方便的对你想要处理的类进行扩展
        Enhancer enhancer = new Enhancer();

        enhancer.setSuperclass(BusinessClass.class);

        //这里增加回调过滤功能啊
        CallbackFilter callbackFilter = new BusinessClassCallBackFilter();

        //过滤规则的约定
        Callback noopCb = NoOp.INSTANCE;
        Callback callback1 = new BusinessInterceptor();
        Callback fixedValue = new TargetResultFixed();
        Callback[] callbacks = new Callback[]{callback1, noopCb, fixedValue};


        enhancer.setCallbacks(callbacks);

        enhancer.setCallbackFilter(callbackFilter);


        //拦截功能的增强,增加过滤器时，注释掉下面一行

//        enhancer.setCallback(new BusinessInterceptor());

        BusinessClass businessClass = (BusinessClass) enhancer.create();

        System.out.println(businessClass);

        businessClass.operate1();

        businessClass.operate2();

        businessClass.operate3();
    }
}
