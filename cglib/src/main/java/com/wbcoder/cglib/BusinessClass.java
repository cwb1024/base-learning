package com.wbcoder.cglib;

/**
  * @description: 这是一个基本的业务逻辑功能类啊
  * @author: chengwb
  * @Date: 2019-11-10 16:00
  */
public class BusinessClass {

    public void operate1(){
        System.out.println("operate method 1 ...");
    }

    public void operate2(){
        System.out.println("operate method 2 ...");
    }

    public void operate3(){
        System.out.println("operate method 3 ...");
    }

    public void operate4(){
        System.out.println("operate method 4 ...");
    }


}
