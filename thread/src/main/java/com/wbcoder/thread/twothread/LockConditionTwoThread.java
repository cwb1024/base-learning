package com.wbcoder.thread.twothread;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class LockConditionTwoThread {

    static Thread t1 = null;
    static Thread t2 = null;
    static ReentrantLock lock = new ReentrantLock();
    static Condition holdA = lock.newCondition();
    static Condition holdB = lock.newCondition();
    static int cnt = 0;

    public static void main(String[] args) {
        t1 = new Thread(() -> {
            for (int i = 0; i < 10; i += 2) {
                lock.lock();
                try {
                    if ((cnt & 1) != 0) {
                        holdA.await();
                    }
                    System.out.print("A");
                    cnt++;
                    holdB.signal();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    lock.unlock();
                }
            }
        });

        t2 = new Thread(() -> {
            for (int i = 1; i < 10; i += 2) {
                lock.lock();
                try {
                    if ((cnt & 1) != 1) {
                        holdB.await();
                    }
                    System.out.print("B");
                    cnt++;
                    holdA.signal();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    lock.unlock();
                }
            }
        });

        t1.start();
        t2.start();
    }
}
