package com.wbcoder.thread.twothread;

public class WaitAndNotifyTwoThread {

    static Thread t1 = null;
    static Thread t2 = null;
    static byte[] lock = new byte[0];
    static int cnt = 0;

    public static void main(String[] args) {
        t1 = new Thread(()->{
            for (int i = 1; i < 10; i+=2) {
                synchronized (lock){
                    if (cnt % 2 != 0){
                        try {
                            lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    System.out.print("A");
                    cnt++;
                    lock.notify();
                }
            }
        });

        t2 = new Thread(()->{
            for (int i = 0; i < 10; i+=2) {
                synchronized (lock) {
                    if (cnt % 2 != 1) {
                        try {
                            lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    System.out.print("B");
                    cnt++;
                    lock.notify();

                }
            }
        });

        t1.start();
        t2.start();
    }
}
