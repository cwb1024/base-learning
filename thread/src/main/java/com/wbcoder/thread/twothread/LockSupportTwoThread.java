package com.wbcoder.thread.twothread;

import java.util.concurrent.locks.LockSupport;

public class LockSupportTwoThread {

    static Thread t1 = null;
    static Thread t2 = null;

    public static void main(String[] args) {
        t1 = new Thread(() -> {
            for (int i = 1; i < 10; i++) {
                System.out.print("A");
                LockSupport.unpark(t2);
                LockSupport.park();
            }
        });

        t2 = new Thread(() -> {
            for (int i = 2; i < 10; i++) {
                LockSupport.park();
                System.out.print("B");
                LockSupport.unpark(t1);
            }
        });

        t1.start();
        t2.start();
    }
}
