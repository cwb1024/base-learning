package com.wbcoder.thread.twothread;

import java.util.concurrent.locks.ReentrantLock;

public class LockStateTwoThread {

    static Thread t1 = null;
    static Thread t2 = null;
    static int cnt = 0;
    static boolean state = true;
    static ReentrantLock lock = new ReentrantLock();

    public static void main(String[] args) {
        t1 = new Thread(() -> {
            lock.lock();
            try {
                while (state) {
                    if ((cnt & 1) == 0) {
                        System.out.print("A");
                        cnt++;
                        state = false;
                    }
                }
            } finally {
                lock.unlock();
            }
        });

        t2 = new Thread(() -> {
            lock.lock();
            try {
                while (!state) {
                    if ((cnt & 1) == 1) {
                        System.out.print("B");
                        cnt++;
                        state = true;
                    }
                }
            } finally {
                lock.unlock();
            }
        });

        t1.start();
        t2.start();
    }
}
