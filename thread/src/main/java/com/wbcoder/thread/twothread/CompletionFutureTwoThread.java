package com.wbcoder.thread.twothread;

import java.util.concurrent.CompletableFuture;

public class CompletionFutureTwoThread {

    static int cnt = 0;
    static byte[] lock = new byte[0];

    public static void main(String[] args) {
        CompletableFuture t1 = CompletableFuture.runAsync(() -> {
            while (cnt < 12){
                synchronized (lock){
                    if((cnt & 1) ==0){
                        System.out.print("A");
                        cnt++;
                    }
                }
            }
        });

        CompletableFuture t2 = CompletableFuture.runAsync(() -> {
            while (cnt < 12){
                synchronized (lock){
                    if ((cnt & 1) == 1){
                        System.out.print("B");
                        cnt++;
                    }
                }
            }
        });

        CompletableFuture.allOf(t1, t2).join();
    }
}
