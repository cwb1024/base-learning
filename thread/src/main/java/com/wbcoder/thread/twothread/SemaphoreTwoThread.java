package com.wbcoder.thread.twothread;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class SemaphoreTwoThread {

    static Thread t1 = null;
    static Thread t2 = null;
    static Thread t3 = null;
    static ReentrantLock lock = new ReentrantLock();

    static Condition holdA = lock.newCondition();
    static Condition holdB = lock.newCondition();
    static Condition holdC = lock.newCondition();

    public static void main(String[] args) {
        t1 = new Thread(() -> {
            for (int i = 1; i < 12; i += 2) {
                try {
                    lock.lock();
                    holdB.signal();
                    System.out.print("A");
                    holdB.await();
                    lock.unlock();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        t2 = new Thread(() -> {
            for (int i = 2; i < 12; i += 2) {
                try {
                    lock.lock();
                    holdC.signal();
                    System.out.print("B");
                    holdB.await();
                    lock.unlock();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        t3 = new Thread(() -> {
            for (int i = 3; i < 12; i += 2) {
                try {
                    lock.lock();
                    holdA.signal();
                    System.out.print("C");
                    holdC.await();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        t1.start();
        t2.start();
        t3.start();
    }

}
