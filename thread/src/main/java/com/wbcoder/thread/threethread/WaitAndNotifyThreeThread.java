package com.wbcoder.thread.threethread;

public class WaitAndNotifyThreeThread {

    static Thread t1 = null;
    static Thread t2 = null;
    static Thread t3 = null;
    static byte[] lock1 = new byte[0];
    static byte[] lock2 = new byte[0];

    public static void main(String[] args) {
        t1 = new Thread(()->{
            synchronized (lock1){
                synchronized (lock2){
                    System.out.println("A");
                }
            }
        });

        t2 = new Thread(()->{
            synchronized (lock1){
                synchronized (lock2){
                    System.out.println("B");
                }
            }
        });

        t3 = new Thread(()->{
            synchronized (lock1){
                synchronized (lock2){
                    System.out.println("C");
                }
            }
        });

        t1.start();
        t2.start();
        t3.start();
    }
}
