package com.wbcoder.thread.threethread;

import java.util.concurrent.locks.LockSupport;

public class LockSupportThreeThread {

    static Thread t1 = null;
    static Thread t2 = null;
    static Thread t3 = null;

    public static void main(String[] args) {
        t1 = new Thread(() -> {
            for (int i = 1; i < 12; i += 2) {
                System.out.print("A");
                LockSupport.unpark(t2);
                LockSupport.park();
            }
        });

        t2 = new Thread(() -> {
            for (int i = 2; i < 12; i += 2) {
                LockSupport.park();
                System.out.print("B");
                LockSupport.unpark(t3);
            }
        });

        t3 = new Thread(() -> {
            for (int i = 3; i < 12; i += 2) {
                LockSupport.park();
                System.out.print("C");
                LockSupport.unpark(t1);
            }
        });

        t1.start();
        t2.start();
        t3.start();
    }
}
