package com.wbcoder.thread.threethread;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class LockConditionThreeThread {

    static int cnt = 0;
    static Thread t1 = null;
    static Thread t2 = null;
    static Thread t3 = null;
    static ReentrantLock lock = new ReentrantLock();

    static Condition holdA = lock.newCondition();
    static Condition holdB = lock.newCondition();
    static Condition holdC = lock.newCondition();

    public static void main(String[] args) {
        t1 = new Thread(() -> {
            for (int i = 1; i < 12; i += 2) {
                lock.lock();
                try {
                    if (cnt % 3 != 0) holdA.await();
                    System.out.print("A");
                    cnt++;
                    holdB.signal();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    lock.unlock();
                }
            }
        });

        t2 = new Thread(() -> {
            for (int i = 2; i < 12; i += 2) {
                lock.lock();
                try {
                    if (cnt % 3 != 1) holdB.await();
                    System.out.print("B");
                    cnt++;
                    holdC.signal();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    lock.unlock();
                }
            }
        });

        t3 = new Thread(() -> {
            for (int i = 3; i < 12; i += 2) {
                lock.lock();
                try {
                    if (cnt % 3 != 2) holdC.await();
                    System.out.print("C");
                    cnt++;
                    holdA.signal();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    lock.unlock();
                }
            }
        });

        t1.start();
        t2.start();
        t3.start();
    }
}
