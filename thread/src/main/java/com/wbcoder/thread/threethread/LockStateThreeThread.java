package com.wbcoder.thread.threethread;

import java.util.concurrent.locks.ReentrantLock;

public class LockStateThreeThread {

    static ReentrantLock lock = new ReentrantLock();
    static int state = 0;

    static Thread t1 = null;
    static Thread t2 = null;
    static Thread t3 = null;

    public static void main(String[] args) {
        t1 = new Thread(() -> {
            for (int i = 1; i < 12; ) {
                lock.lock();
                while (state % 3 == 0) {
                    System.out.print("A");
                    state++;
                    i += 2;
                }
                lock.unlock();
            }
        });

        t2 = new Thread(() -> {
            for (int i = 2; i < 12; ) {
                lock.lock();
                while (state % 3 == 1) {
                    System.out.print("B");
                    state++;
                    i += 2;
                }
                lock.unlock();
            }
        });

        t3 = new Thread(() -> {
            for (int i = 3; i < 12; ) {
                lock.lock();
                while (state % 3 == 2) {
                    System.out.print("C");
                    state++;
                    i += 2;
                }
                lock.unlock();
            }
        });

        t1.start();
        t2.start();
        t3.start();
    }
}
