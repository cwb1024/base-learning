package com.wbcoder.thread.threethread;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicIntegerThreeThread {

    static AtomicInteger num = new AtomicInteger();
    static int max = 12;

    static Thread t1 = null;
    static Thread t2 = null;
    static Thread t3 = null;

    public static void main(String[] args) {
        t1 = new Thread(() -> {
            while (num.get() < max) {
                if(num.get() % 3 == 0){
                    System.out.print("A");
                    num.incrementAndGet();
                }
            }
        });

        t2 = new Thread(() -> {
            while (num.get() < max) {
                if(num.get() % 3 == 1){
                    System.out.print("B");
                    num.incrementAndGet();
                }
            }
        });

        t3 = new Thread(() -> {
            while (num.get() < max) {
                if(num.get() % 3 == 2){
                    System.out.print("C");
                    num.incrementAndGet();
                }
            }
        });

        t1.start();
        t2.start();
        t3.start();
    }
}
