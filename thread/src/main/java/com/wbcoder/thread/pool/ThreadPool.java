package com.wbcoder.thread.pool;

import java.util.Map;
import java.util.concurrent.*;

/**
 * 线程池缓存
 */
public class ThreadPool {

    private static Map<String, ThreadPoolExecutor> map = new ConcurrentHashMap<>();
    private ThreadPoolExecutor executor;

    /**
     * 阻塞任务队列数
     */
    private int waitingCount;
    /**
     * 线程池的名字
     */
    @SuppressWarnings("unused")
    private String name;

    public ThreadPool(String name, int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit,
                      BlockingQueue<Runnable> workQueue) {
        synchronized (map) {
            this.name = name;
            this.waitingCount = workQueue.size();
            String key = buildKey(name, corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue.size(), "#");
            if (map.containsKey(key)) {
                executor = map.get(key);
            } else {
                executor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
                map.put(key, executor);
            }
        }
    }

    public ThreadPool(String name, int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit,
                      int waitingCount) {
        synchronized (map) {
            this.name = name;
            this.waitingCount = (int) (waitingCount * 1.5);
            String key = buildKey(name, corePoolSize, maximumPoolSize, keepAliveTime, unit, waitingCount, "#");
            if (map.containsKey(key)) {
                executor = map.get(key);
            } else {
                executor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize, keepAliveTime, unit,
                        new LinkedBlockingQueue<Runnable>(this.waitingCount));
                map.put(key, executor);
            }
        }
    }

    private String buildKey(String name, int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit,
                            int waitingCount, String delimiter) {
        StringBuilder result = new StringBuilder();
        result.append(name).append(delimiter);
        result.append(corePoolSize).append(delimiter);
        result.append(maximumPoolSize).append(delimiter);
        result.append(keepAliveTime).append(delimiter);
        result.append(unit.toString()).append(delimiter);
        result.append(waitingCount);
        return result.toString();
    }


    public void execute(Runnable runnable) {
        checkQueueSize();
        executor.execute(runnable);
    }

    private void checkQueueSize() {
        while (getTaskSize() >= waitingCount) {//如果线程池中的阻塞队列数 > waitingCount 则继续等待
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public int getTaskSize() {
        return executor.getQueue().size();
    }
}
