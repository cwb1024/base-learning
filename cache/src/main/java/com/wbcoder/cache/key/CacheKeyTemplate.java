package com.wbcoder.cache.key;

/**
 * 缓存key模版
 */
public class CacheKeyTemplate {

    /**
     * 用户信息
     */
    public static String USER_INFO = "user_info_{user_id}";
    /**
     * 用户登陆标记
     */
    public static String USER_LOGIN_TODAY = "user_login_today_{user_id}_{today}";

}
