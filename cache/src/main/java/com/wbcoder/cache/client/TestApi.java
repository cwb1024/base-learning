package com.wbcoder.cache.client;

import com.google.common.cache.*;
import com.wbcoder.cache.model.CacheModel;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class TestApi {

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        //缓存接口这里是LoadingCache,LoadingCache在缓存项不存在的时候，可以自动加载缓存。


        LoadingCache<String, CacheModel> loadingCache =
                //CacheBuilder的构造方法是私有的，只能通过静态方法newBuilder()来获得CacheBuilder的实例。
                CacheBuilder.newBuilder()
                        //设置并发级别为8，并发级别指的是可以同时写缓存的线程数
                        .concurrencyLevel(8)
                        //写入缓存 8 s  自动丢失
                        .expireAfterWrite(8, TimeUnit.SECONDS)
                        //写入缓存 1 s  刷新
                        .refreshAfterWrite(1, TimeUnit.SECONDS)
                        //初始化缓存容器的初始容量为 10
                        .initialCapacity(10)
                        //容器内最大缓存的容量为 100 ，超过100 后就会按照 LRU 最近最少使用算法来移除缓存项
                        .maximumSize(100)
                        //设置要记录缓存的命中率
                        .recordStats()
                        //设置缓存的移除通知
                        .removalListener(new RemovalListener<Object, Object>() {
                            @Override
                            public void onRemoval(RemovalNotification<Object, Object> notification) {
                                System.out.println("-------" + notification.getKey() + " was removed, cause is" + notification.getCause());
                            }
                        })
                        //build方法中可以指定CacheLoader,在缓存不存在的时候会自动加载缓存
                        .build(new CacheLoader<String, CacheModel>() {
                            @Override
                            public CacheModel load(String key) throws Exception {
                                System.out.println("load CacheModel " + key);
                                CacheModel cacheModel = new CacheModel();
                                cacheModel.setId("resourceId");
                                cacheModel.setName("resourceName");
                                return cacheModel;
                            }
                        });

        for (int i = 0; i < 20; i++) {
            long start = System.currentTimeMillis();
            CacheModel cacheModel = loadingCache.get(i + "");
            System.out.println(cacheModel.toString());

            Thread.sleep(1000);
            long end = System.currentTimeMillis();
            System.out.println("----️" + (end - start) + " ms");

        }


    }
}
