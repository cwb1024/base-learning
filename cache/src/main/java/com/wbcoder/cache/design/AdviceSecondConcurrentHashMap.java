package com.wbcoder.cache.design;

import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;


/**
 *
 * 单纯的想缓存一部分数据，减少IO，直接操作单机内存，保证只请求一次IO，高效的实现
 *
 *
 *
 * 使用单机缓存，也就是本地缓存的目的，就是将一些不经常变动的数据，缓存起来，减少网络IO，以及数据库数据的读取
 * threadName 的传入 是为了打印日志
 * 我们为了实现性能和缓存的结果，我们采用Future，因为Future在计算完成时获取，否则会一直阻塞直到任务转入完成状态和ConcurrentHashMap.putIfAbsent方法
 *
 * 线程T1或者线程T2访问cacheMap，如果都没有时，这时执行了FutureTask来完成异步任务，
 * 假如线程T1执行了FutureTask，并把保存到ConcurrentHashMap中，通过PutIfAbsent方法，
 * 因为putIfAbsent方法如果不存在key对应的值，则将value以key加入Map，否则返回key对应的旧值。
 * 这时线程T2进来时可以获取Future对象，如果没值没关系，这时是对象的引用，等FutureTask执行完，在通过get返回。
 * @param <K> key 是一致的
 * @param <V> 通过业务执行读取到的资源数据，准备缓存的数据
 */
public class AdviceSecondConcurrentHashMap<K, V> {

    //多线程共享的缓存数据变量，全局一份，只读不修改，没有并发问题，写的时候有没有并发问题呢
    private final ConcurrentHashMap<K, Future<V>> cacheMap = new ConcurrentHashMap<>();


    //读取缓存
    public synchronized Object getCache(K keyValue, String threadName) {

        Future<V> value = null;

        try {

            System.out.println("threadName get cache ========" + threadName);

            //从缓存中获取数据

            value = cacheMap.get(keyValue);

            if (value == null) {

                putValue(keyValue, threadName);

                return value.get();
            }
        } catch (Exception e) {

        }

        return null;
    }

    //设置缓存
    public Future<V> putValue(K keyValue, String threadName) {

        Future<V> value = null;

        Callable<V> callable = new Callable<V>() {
            @Override
            public V call() throws Exception {
                System.out.println("多线程环境下，当前缓存读取数据时，缓存失效，或者没有，请求网络资源，读取数据，并且放到缓存内，当前线程执行者：" + threadName);

                //通过业务执行读取到的资源数据，准备缓存的数据
                return  (V) "resourceDataValue";
            }
        };

        FutureTask<V> futureTask = new FutureTask<V>(callable);

        value = cacheMap.putIfAbsent(keyValue, futureTask);

        if (value == null) {
            value = futureTask;

            futureTask.run();
        }
        return value;

    }


    public static void main(String[] args) throws Exception {

        final AdviceSecondConcurrentHashMap baseVersionConcurrentHashMap = new AdviceSecondConcurrentHashMap();

        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("=====thread1 start =====");

                Object value = baseVersionConcurrentHashMap.getCache("key", "T1");

                System.out.println("======T1 value ======== " + value);

                System.out.println("======T1 end =====");
            }
        });

        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("=====thread2 start =====");

                Object value = baseVersionConcurrentHashMap.getCache("key", "T2");

                System.out.println("======T2 value ======== " + value);

                System.out.println("======T2 end =====");
            }
        });

        Thread thread3 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("=====thread3 start =====");

                Object value = baseVersionConcurrentHashMap.getCache("key", "T3");

                System.out.println("======T3 value ======== " + value);

                System.out.println("======T3 end =====");
            }
        });

        //当前成同时执行时，可能存在还是调用了多次网络资源、或者数据库 存在，不够完美
        thread1.start();

        thread2.start();

        thread3.start();

    }
}
