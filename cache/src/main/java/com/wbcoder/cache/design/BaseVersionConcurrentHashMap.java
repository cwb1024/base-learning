package com.wbcoder.cache.design;

import java.util.concurrent.ConcurrentHashMap;


/**
 *
 * 单纯的想缓存一部分数据，减少IO，直接操作单机内存
 *
 *
 * 使用单机缓存，也就是本地缓存的目的，就是将一些不经常变动的数据，缓存起来，减少网络IO，以及数据库数据的读取
 * 并发读不会出问题，因为读到的资源都是别人返回的，但是并发时存在还是读了多次的问题
 * threadName 的传入 是为了打印日志
 * @param <K> key 是一致的
 * @param <V> 通过业务执行读取到的资源数据，准备缓存的数据
 */
public class BaseVersionConcurrentHashMap<K,V> {

    //多线程共享的缓存数据变量，全局一份，只读不修改，没有并发问题，写的时候有没有并发问题呢
    private final ConcurrentHashMap<K, V> cacheMap = new ConcurrentHashMap<>();


    //读取缓存
    public  Object getCache(K keyValue, String threadName) {
        System.out.println("threadName get cache ========" + threadName);

        Object value = null;

        //从缓存中获取数据

        value = cacheMap.get(keyValue);

        if (value == null) {

            return putValue(keyValue,threadName);
        }

        return value;
    }

    //设置缓存
    public Object putValue(K keyValue, String threadName) {

        System.out.println("多线程环境下，当前缓存读取数据时，缓存失效，或者没有，请求网络资源，读取数据，并且放到缓存内，当前线程执行者："+threadName);

        //通过业务执行读取到的资源数据，准备缓存的数据
        V value = (V) "resourceDataValue";

        //把数据存到缓存
        cacheMap.put(keyValue, value);

        return value;

    }
    

    public static void main(String[] args) throws Exception{

        final BaseVersionConcurrentHashMap baseVersionConcurrentHashMap = new BaseVersionConcurrentHashMap();

        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("=====thread1 start =====");

                Object value = baseVersionConcurrentHashMap.getCache("key", "T1");

                System.out.println("======T1 value ======== "+ value);

                System.out.println("======T1 end =====");
            }
        });

        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("=====thread2 start =====");

                Object value = baseVersionConcurrentHashMap.getCache("key", "T2");

                System.out.println("======T2 value ======== "+ value);

                System.out.println("======T2 end =====");
            }
        });

        Thread thread3 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("=====thread3 start =====");

                Object value = baseVersionConcurrentHashMap.getCache("key", "T3");

                System.out.println("======T3 value ======== "+ value);

                System.out.println("======T3 end =====");
            }
        });

        //当前成同时执行时，可能存在还是调用了多次网络资源、或者数据库 存在，不够完美
        thread1.start();

        thread2.start();

        thread3.start();

    }
}
