package com.wbcoder.cache.design;

import java.util.concurrent.ConcurrentHashMap;


/**
 *
 * 单纯的想缓存一部分数据，减少IO，直接操作单机内存，保证只请求一次IO，低效的实现，霸道的实现
 *
 *
 * 使用单机缓存，也就是本地缓存的目的，就是将一些不经常变动的数据，缓存起来，减少网络IO，以及数据库数据的读取
 * threadName 的传入 是为了打印日志
 * 加锁保证，只需要请求一次网络资源或者数据库IO，通过强势的 synchronized 关键字 保证读只能 顺序读 第一次读完放进去，别人都能用，但是 synchronized 是有点低效的
 * @param <K> key 是一致的
 * @param <V> 通过业务执行读取到的资源数据，准备缓存的数据
 */
public class AdviceFirstConcurrentHashMap<K,V> {

    //多线程共享的缓存数据变量，全局一份，只读不修改，没有并发问题，写的时候有没有并发问题呢
    private final ConcurrentHashMap<K, V> cacheMap = new ConcurrentHashMap<>();

    //读取缓存
    public  synchronized Object getCache(K keyValue, String threadName) {
        System.out.println("threadName get cache ========" + threadName);

        Object value = null;

        //从缓存中获取数据

        value = cacheMap.get(keyValue);

        if (value == null) {

            return putValue(keyValue,threadName);
        }

        return value;
    }



    //设置缓存
    public Object putValue(K keyValue, String threadName) {

        System.out.println("多线程环境下，当前缓存读取数据时，缓存失效，或者没有，请求网络资源，读取数据，并且放到缓存内，当前线程执行者："+threadName);

        //通过业务执行读取到的资源数据，准备缓存的数据
        V value = (V) "resourceDataValue";

        //把数据存到缓存
        cacheMap.put(keyValue, value);

        return value;

    }


    /**
     * main函数模拟tomcat多线程环境，模拟多个请求线程
     */
    public static void main(String[] args) throws Exception{

        final AdviceFirstConcurrentHashMap baseVersionConcurrentHashMap = new AdviceFirstConcurrentHashMap();

        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("=====thread1 start =====");

                Object value = baseVersionConcurrentHashMap.getCache("key", "T1");

                System.out.println("======T1 value ======== "+ value);

                System.out.println("======T1 end =====");
            }
        });

        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("=====thread2 start =====");

                Object value = baseVersionConcurrentHashMap.getCache("key", "T2");

                System.out.println("======T2 value ======== "+ value);

                System.out.println("======T2 end =====");
            }
        });

        Thread thread3 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("=====thread3 start =====");

                Object value = baseVersionConcurrentHashMap.getCache("key", "T3");

                System.out.println("======T3 value ======== "+ value);

                System.out.println("======T3 end =====");
            }
        });

        //当前成同时执行时，可能存在还是调用了多次网络资源、或者数据库 存在，不够完美
        thread1.start();

        thread2.start();

        thread3.start();

    }
}
