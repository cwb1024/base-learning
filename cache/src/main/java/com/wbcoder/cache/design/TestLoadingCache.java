package com.wbcoder.cache.design;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import java.util.concurrent.ExecutionException;

public class TestLoadingCache {

    public static void main(String[] args) throws ExecutionException {


        CacheLoader<String,String> loader = new CacheLoader<String, String>() {
            @Override
            public String load(String key) throws Exception {

                //模拟数据，调用数据接口
                Thread.sleep(1000);

                System.out.println(key + " is loaded from a cacheLoader");
                return key + "'s value";
            }
        };

        //在构建时指定自动加载器
        LoadingCache loadingCache = CacheBuilder.newBuilder().maximumSize(3).build(loader);

        loadingCache.get("key1");

        loadingCache.get("key2");

        loadingCache.get("key3");
    }
}
