package com.wbcoder.cache.design;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

public class TestGuavaCache<K,V> {

    private Cache<K, V> cache = CacheBuilder.newBuilder().maximumSize(2).expireAfterAccess(10, TimeUnit.MINUTES).build();

    public Object getCache(K keyValue, final String ThreadName) {

        Object value = null ;

        try {

            System.out.println("threadName get cache ======" + ThreadName);

            //从缓存内获取数据
            value = cache.get(keyValue, new Callable<V>() {
                @Override
                public V call() throws Exception {
                    System.out.println("多线程环境下，当前缓存读取数据时，缓存失效，或者没有，请求网络资源，读取数据，并且放到缓存内，当前线程执行者：" + ThreadName);

                    //通过业务执行读取到的资源数据，准备缓存的数据
                    return  (V) "resourceDataValue";
                }
            });

        } catch (Exception e) {

        }
        return null;
    }

    public static void main(String[] args) {

        TestGuavaCache cache = new TestGuavaCache();

        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("=====thread1 start =====");

                Object value = cache.getCache("key", "T1");

                System.out.println("======T1 value ======== " + value);

                System.out.println("======T1 end =====");
            }
        });

        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("=====thread2 start =====");

                Object value = cache.getCache("key", "T2");

                System.out.println("======T2 value ======== " + value);

                System.out.println("======T2 end =====");
            }
        });

        Thread thread3 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("=====thread3 start =====");

                Object value = cache.getCache("key", "T3");

                System.out.println("======T3 value ======== " + value);

                System.out.println("======T3 end =====");
            }
        });

        //当前成同时执行时，可能存在还是调用了多次网络资源、或者数据库 存在，不够完美
        thread1.start();

        thread2.start();

        thread3.start();
    }

}
