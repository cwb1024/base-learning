package tomcat;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 一请求一线程方式
 */
public class BIOServer {

    public static void main(String[] args) throws Exception {

        ExecutorService executor = Executors.newFixedThreadPool(2000);


        //主线程用来监听客户端需要连接上来的端口

        ServerSocket serverSocket = new ServerSocket(9000);

        while (true) {

            //这里主线程阻塞等待，用于等待新的连接到来，然后给他开启一个线程进行处理
            Socket socket = serverSocket.accept();

            //这里对客户端socket进行读写是阻塞的，为了不阻塞主线程能够接受新的请求连接，这里把这个阻塞流程放到子线程内
            //子线程对连接进行处理
            executor.submit(new WorkerThreadHandler(socket));

        }
    }

    /**
     * 开启一个工作线程进行处理连接上来的连接
     */
    static class WorkerThreadHandler implements Runnable {

        Socket socket = null;

        public WorkerThreadHandler(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {

            //负责处理这个客户端socket的读写操作

            try {
                InputStream inputStream = socket.getInputStream();


                inputStream.read();

                //进行读写操作


            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
