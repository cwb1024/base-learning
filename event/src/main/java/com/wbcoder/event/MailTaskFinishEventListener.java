package com.wbcoder.event;

/**
 * @description: 邮箱对任务的完成状态的监听
 * @author: chengwb
 * @Date: 2020/5/3 22:03
 */
public class MailTaskFinishEventListener implements TaskFinishEventListener {

    private String email;

    public MailTaskFinishEventListener(String email) {
        this.email = email;
    }

    @Override
    public void onTaskFinish(TaskFinishEvent event) {
        System.out.println(String.format("邮箱事件监听到任务事件已经完成了，发送 email %s , Task : %s",
                email,
                event.getSource())
        );
    }
}
