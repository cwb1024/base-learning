package com.wbcoder.event;

public class SmsTaskFinishEventListener implements TaskFinishEventListener {

    public String address;

    public SmsTaskFinishEventListener(String address) {
        this.address = address;
    }

    @Override
    public void onTaskFinish(TaskFinishEvent event) {
        System.out.printf(
                "Send message to %s , Task : %s%n",
                address,
                event
        );
    }
}
