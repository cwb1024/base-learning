package com.wbcoder.event;

/**
 * @description: 任务完成的状态枚举状态
 * @author: chengwb
 * @Date: 2020/5/3 21:58
 */
public enum TaskFinishEnums {

    SUCCEED, FAIL;
}
