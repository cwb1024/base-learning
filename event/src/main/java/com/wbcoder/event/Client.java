package com.wbcoder.event;

/**
 * @description: 这里搞一个测试类，对模拟的场景进行一个测试
 * <p>
 * 场景类里边对事件的发生的生命周期进行了控制，并且主动的进行了事件的发布，监听器的注册
 * <p>
 * 这个过程内 JDK提供的两个类充当了什么作用？
 * <p>
 * eventObject类：用户所有自定义事件的基类
 * eventListener接口：A tagging interface that all event listener interfaces must extend
 * @author: chengwb
 * @Date: 2020/5/3 22:12
 */
public class Client {

    public static void main(String[] args) {

        //事件源
        Task source = new Task("我的自定义事件", TaskFinishEnums.SUCCEED);

        //任务结束事件
        TaskFinishEvent event = new TaskFinishEvent(source);

        //邮件服务监听器
        MailTaskFinishEventListener listener = new MailTaskFinishEventListener("www.abc.com");

        //这里扩展一个监听任务结束后，发送一条短信的消息监听器
        SmsTaskFinishEventListener smsTaskFinishEventListener = new SmsTaskFinishEventListener("xxx");

        //事件发布器
        TaskFinishEventPublisher publisher = new TaskFinishEventPublisher();

        //注册邮件服务监听器
        publisher.register(listener);

        publisher.register(smsTaskFinishEventListener);

        //发布事件
        publisher.publishEvent(event);
    }
}
