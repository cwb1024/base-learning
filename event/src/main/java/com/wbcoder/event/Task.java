package com.wbcoder.event;


/**
  * @description: 这里模拟一个自定义的任务
  * @author: chengwb
  * @Date: 2020/5/3 21:55
  */
public class Task {

    private String name;

    private TaskFinishEnums status;

    public Task(String name, TaskFinishEnums succeed) {
        this.name = name;
        this.status = succeed;
    }

    @Override
    public String toString() {
        return "Task{" +
                "name='" + name + '\'' +
                ", status=" + status +
                '}';
    }
}
