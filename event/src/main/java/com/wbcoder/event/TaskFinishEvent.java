package com.wbcoder.event;

import java.util.EventObject;

/**
 * @description: 任务结束事件TaskFinishEvent
 * @author: chengwb
 * @Date: 2020/5/3 22:00
 */
public class TaskFinishEvent extends EventObject {
    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    public TaskFinishEvent(Object source) {
        super(source);
    }
}
