package com.wbcoder.event;

import java.util.EventListener;


/**
 * @description: 事件监听，继承一个空的接口
 * @author: chengwb
 * @Date: 2020/5/3 22:41
 */
public interface TaskFinishEventListener extends EventListener {

    /**
     * 这里是自定义的一个事件动作
     *
     * @param event
     */
    void onTaskFinish(TaskFinishEvent event);
}
