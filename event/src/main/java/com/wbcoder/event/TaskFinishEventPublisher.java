package com.wbcoder.event;

import java.util.ArrayList;
import java.util.List;

/**
 * @description: 这里自定义一个事件的发布器
 * @author: chengwb
 * @Date: 2020/5/3 22:07
 */

public class TaskFinishEventPublisher {

    private List<TaskFinishEventListener> listeners = new ArrayList<>();


    //注册监听器
    public synchronized void register(TaskFinishEventListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    //移除监听器
    public synchronized boolean remove(TaskFinishEventListener listener) {
        return listeners.remove(listener);
    }

    //发布任务结束事件
    public void publishEvent(TaskFinishEvent event) {
        for (TaskFinishEventListener listener : listeners) {
            listener.onTaskFinish(event);
        }
    }
}
