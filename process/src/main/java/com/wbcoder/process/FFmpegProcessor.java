package com.wbcoder.process;


import com.wbcoder.process.base.KVConfig;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * FFmpeg进程处理器
 */
@Slf4j
@Component
public class FFmpegProcessor {

    @Autowired
    private KVConfig kvConfig;

    /**
     * 执行一批命令
     *
     * @param cmds
     * @return
     * @throws Exception
     */
    public String execute(List<String> cmds) throws Exception {
        if (CollectionUtils.isEmpty(cmds)) {
            return null;
        }
        LinkedList<String> commands = new LinkedList<>(cmds);
        commands.addFirst(kvConfig.ffmpegPath);

        Runtime runtime = Runtime.getRuntime();
        Process ffmpeg = null;
        try {
            ProcessBuilder builder = new ProcessBuilder();
            builder.command(commands);
            ffmpeg = builder.start();

            /**
             * 取出输出流和错误流的信息
             *
             * 注意：必须要取出ffmpeg在执行命令过程中产生的输出信息，
             *      如果不取的话当输出流信息填满jvm存储输出留信息的缓冲区时，线程就回阻塞住
             */
            PrintStream errorStream = new PrintStream(ffmpeg.getErrorStream());
            PrintStream inputStream = new PrintStream(ffmpeg.getInputStream());
            errorStream.start();
            inputStream.start();
            // 等待命令执行完
            ffmpeg.waitFor();
            // 获取执行结果字符串
            String result = errorStream.stringBuffer.append(inputStream.stringBuffer).toString();
            // 输出执行的命令信息
            String cmdStr = Arrays.toString(commands.toArray()).replace(",", "");
            String resultStr = StringUtils.isBlank(result) ? "异常" : "正常";
            //log.info("媒体处理执行结果:{}, 命令行:{}", resultStr, cmdStr);
            return result;
        } catch (Exception e) {
            //log.error("媒体处理执行出错:{}", e.getMessage());
            return null;
        } finally {
            if (null != ffmpeg) {
                ProcessKiller killer = new ProcessKiller(ffmpeg);
                // JVM退出时，先通过钩子关闭FFmepg进程
                runtime.addShutdownHook(killer);
            }
        }
    }

    /**
     * 在程序退出前结束已有的FFmpeg进程
     */
    private static class ProcessKiller extends Thread {
        private Process process;

        public ProcessKiller(Process process) {
            this.process = process;
        }

        @Override
        public void run() {
            this.process.destroy();
            //log.info("已销毁FFmpeg进程:" + process.toString());
        }
    }

    /**
     * 用于取出ffmpeg线程执行过程中产生的各种输出和错误流的信息
     */
    static class PrintStream extends Thread {
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        StringBuffer stringBuffer = new StringBuffer();

        public PrintStream(InputStream inputStream) {
            this.inputStream = inputStream;
        }

        @Override
        public void run() {
            try {
                if (null == inputStream) {
                    //log.error("读取输出流出错！因为当前输出流为空");
                }
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String line = null;
                while ((line = bufferedReader.readLine()) != null) {
                    //log.info(line);
                    stringBuffer.append(line);
                }
            } catch (Exception e) {
                //log.error("--- 读取输入流出错了！--- 错误信息：" + e.getMessage());
            } finally {
                try {
                    if (null != bufferedReader) {
                        bufferedReader.close();
                    }
                    if (null != inputStream) {
                        inputStream.close();
                    }
                } catch (IOException e) {
                    //log.error("--- 调用PrintStream读取输出流后，关闭流时出错！---");
                }
            }
        }
    }
}
