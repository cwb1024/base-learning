package com.wbcoder.exception.handler;

import java.util.Objects;

/**
 * WXG BOT https://work.weixin.qq.com/api/doc/90000/90136/91770
 */
public class WXGAppender {

    /**
     * 默认无配色黑色文本
     *
     * @param builder
     * @param key
     * @param value
     */
    public static void text(StringBuilder builder, String key, String value) {
        builder.append("\n");
        builder.append(key);
        builder.append(": ");
        builder.append(value);
    }

    /**
     * <font color="comment">灰色</font>
     *
     * @param builder
     * @param key
     * @param value
     */
    public static void comment(StringBuilder builder, String key, String value) {
        builder.append("\n");
        builder.append(key);
        builder.append(": ");
        builder.append("<font color=\"comment\">");
        builder.append(value);
        builder.append("</font>");
    }

    /**
     * > 引用文字
     *
     * @param builder
     * @param key
     * @param value
     */
    public static void rel(StringBuilder builder, String key, String value) {
        builder.append("\n");
        builder.append(">");
        builder.append(key);
        builder.append(": ");
        builder.append("<font color=\"comment\">");
        builder.append(value);
        builder.append("</font>");
    }

    /**
     * <font color="warning">橙红色</font>
     *
     * @param builder
     * @param key
     * @param value
     */
    public static void warn(StringBuilder builder, String key, String value) {
        builder.append("\n");
        builder.append("<font color=\"warning\">");
        builder.append(key);
        builder.append("</font>");
        builder.append(": ");
        builder.append("<font color=\"comment\">");
        builder.append(value);
        builder.append("</font>");
    }

    /**
     * <font color="info">绿色</font>
     *
     * @param builder
     * @param key
     * @param value
     */
    public static void info(StringBuilder builder, String key, String value) {
        builder.append("\n");
        builder.append("<font color=\"info\">");
        builder.append(key);
        builder.append("</font>");
        builder.append(": ");
        builder.append("<font color=\"comment\">");
        builder.append(value);
        builder.append("</font>");
    }


    /**
     * 标题 （支持1至6级标题，注意#与文字中间要有空格）
     *
     * @param builder
     * @param key
     * @param value
     */
    public static void title(StringBuilder builder, String key, String value) {
        builder.append("\n");
        builder.append("###### ");
        builder.append("<font color=\"warning\">");
        builder.append(key);
        builder.append("</font>");
        builder.append(": ");
        builder.append("<font color=\"comment\">");
        builder.append(value);
        builder.append("</font>");
    }

    /**
     * 行内代码段
     *
     * @param builder
     * @param key
     * @param value
     */
    public static void codeBlock(StringBuilder builder, String key, String value) {
        builder.append("\n");
        builder.append(key);
        builder.append(": ");
        builder.append("`");
        builder.append(value);
        builder.append("`");
    }


    /**
     * 加粗
     *
     * @param builder
     * @param key
     * @param value
     */
    public static void bold(StringBuilder builder, String key, String value) {
        builder.append("\n");
        builder.append(key);
        builder.append(": ");
        builder.append("**");
        builder.append(value);
        builder.append("**");
    }

    /**
     * 格式化打印详情
     *
     * @param e 异常对象
     * @return string
     */
    public static String printf(Exception e) {
        StackTraceElement stack = e.getStackTrace()[0];
        if (Objects.isNull(stack)) {
            return "";
        }
        StringBuilder builder = new StringBuilder();
        comment(builder, "文件名", stack.getFileName());
        comment(builder, "类名", stack.getClassName());
        comment(builder, "方法名", stack.getMethodName());
        bold(builder, "行号", String.valueOf(stack.getLineNumber()));
        comment(builder, "detailMessage", e.getMessage());
        return builder.toString();
    }


    private static final String appId = null;
    private static final String env = null;
    private void appendMsg(StringBuilder builder,String scope, Exception e, String title, String path, Object requestParams, String now) {
        WXGAppender.title(builder, "标题", title);
        WXGAppender.rel(builder, "接口", path);
        //WXGAppender.codeBlock(builder, "参数", JSON.toJSONString(requestParams));
        WXGAppender.comment(builder, "依赖", scope);
        WXGAppender.text(builder, "详情", WXGAppender.printf(e));
        WXGAppender.info(builder, "服务", appId);
        WXGAppender.info(builder, "环境", env);
        //WXGAppender.info(builder, "机器", getHostIp());
        WXGAppender.info(builder, "时间", now);
    }
}
