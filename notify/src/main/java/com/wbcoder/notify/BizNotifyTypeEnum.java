package com.wbcoder.notify;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum BizNotifyTypeEnum {
    WXG("wxg");
    private String code;
}
