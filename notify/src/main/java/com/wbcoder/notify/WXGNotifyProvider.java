package com.wbcoder.notify;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wbcoder.thread.pool.DefaultThreadPoolManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;

/**
 * 微信通知提供者
 */
@Slf4j
@Component
public class WXGNotifyProvider extends BizNotifyStrategy {

    private static final String appId = "appId";
    private final String notifyUrl = "notifyUrl";
    private final String env = "env";

    @Autowired
    private RestTemplate restTemplate;

    /**
     * 告警
     */
    @Override
    public void alarm(BizNotifyContext context) {
        httpPost(context);
    }

    /**
     * 异步告警
     */
    @Override
    public void alarmAsync(BizNotifyContext context) {
        DefaultThreadPoolManager.getInstance().execute(() -> {
            httpPost(context);
        });
    }

    private void httpPost(BizNotifyContext context) {
        URI uri;
        try {
            uri = new URI(notifyUrl);
        } catch (URISyntaxException e) {
            log.error("WXG talk uri error,url:{}", notifyUrl, e);
            throw new RuntimeException("WXG talk uri to url error.");
        }
        JSONObject json = buildMessageTemplate(context);
        HttpHeaders httpHeaders = new HttpHeaders();
        MediaType type = MediaType.parseMediaType("application/json;charset=UTF-8");
        httpHeaders.setContentType(type);
        HttpEntity<String> httpEntity = new HttpEntity<>(json.toString(), httpHeaders);
        restTemplate.postForEntity(uri, httpEntity, String.class);
    }

    private JSONObject buildMessageTemplate(BizNotifyContext context) {
        JSONObject json = new JSONObject();
        json.put("msgtype", "markdown");
        JSONObject markdown = new JSONObject();
        markdown.put("content", buildContentTemplate(context));
        json.put("markdown", markdown);
        return json;
    }

    private String buildContentTemplate(BizNotifyContext context) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        StringBuilder builder = new StringBuilder();
        appendMsg(builder, context, context.getTitle(), context.getPath(), context.getRequestParams(), sdf.format(System.currentTimeMillis()));
        return builder.toString();
    }

    private void appendMsg(StringBuilder builder, BizNotifyContext context, String title, String path, Object requestParams, String now) {
        WXGAppender.title(builder, "标题", title);
        WXGAppender.rel(builder, "接口", path);
        WXGAppender.codeBlock(builder, "参数", JSON.toJSONString(requestParams));
        WXGAppender.comment(builder, "依赖", context.getScope());
        if (context.getBizException() != null) {
            WXGAppender.text(builder, "详情", WXGAppender.printf(context.getBizException()));
        } else if (context.getUnknownException() != null) {
            WXGAppender.text(builder, "详情", WXGAppender.printf(context.getUnknownException()));
        }
        WXGAppender.info(builder, "服务", appId);
        WXGAppender.info(builder, "环境", env);
        WXGAppender.info(builder, "机器", getHostIp());
        WXGAppender.info(builder, "时间", now);
    }
}
