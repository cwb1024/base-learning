package com.wbcoder.notify;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.Objects;

@Slf4j
@Component
public abstract class BizNotifyStrategy {

    public abstract void alarm(BizNotifyContext context);

    public abstract void alarmAsync(BizNotifyContext context);

    /**
     * 获取本机ip地址
     *
     * @return ip地址
     */
    protected String getHostIp() {
        String tempIp = "127.0.0.1";
        try {
            tempIp = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            log.info("getHostIP error.", e);
        }
        try {
            Enumeration<NetworkInterface> networks = NetworkInterface.getNetworkInterfaces();
            InetAddress ip = null;
            Enumeration<InetAddress> addrs;
            while (networks.hasMoreElements()) {
                addrs = networks.nextElement().getInetAddresses();
                while (addrs.hasMoreElements()) {
                    ip = addrs.nextElement();
                    if (Objects.nonNull(ip) && ip instanceof Inet4Address && ip.isSiteLocalAddress() && !ip.getHostAddress().equals(tempIp)) {
                        return ip.getHostAddress();
                    }
                }
            }
            return tempIp;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
