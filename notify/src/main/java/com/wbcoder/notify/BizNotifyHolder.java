package com.wbcoder.notify;

import com.alibaba.fastjson.JSON;
import com.wbcoder.notify.constans.BizException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Component
public class BizNotifyHolder implements InitializingBean {

    private final Map<String, BizNotifyStrategy> map = new ConcurrentHashMap<>();

    @Autowired
    private WXGNotifyProvider wxgNotifyProvider;

    @Override
    public void afterPropertiesSet() throws Exception {
        map.put(BizNotifyTypeEnum.WXG.getCode(), wxgNotifyProvider);
    }

    /**
     * 处理
     *
     * @param context 上下文
     */
    public void handle(BizNotifyContext context) {
        try {
            BizNotifyStrategy strategy = getBizNotifyStrategy(context);
            strategy.alarm(context);
        } catch (Exception e) {
            log.error("处理失败.", e);
        }
    }

    /**
     * 异步处理
     *
     * @param context 上下文
     */
    public void handleAsync(BizNotifyContext context) {
        try {
            BizNotifyStrategy strategy = getBizNotifyStrategy(context);
            strategy.alarmAsync(context);
        } catch (Exception e) {
            log.error("异步处理失败.", e);
        }
    }

    private BizNotifyStrategy getBizNotifyStrategy(BizNotifyContext context) {
        if (context == null || !StringUtils.hasLength(context.getNotifyType())) {
            String message = "BizNotifyHolder.handle fail. msg=引擎类型为空. context=" + JSON.toJSONString(context);
            throw new BizException(message);
        }
        BizNotifyStrategy strategy = map.get(context.getNotifyType());
        if (strategy == null) {
            String message = "BizNotifyHolder.handle fail. msg=未找到处理策略. context=" + JSON.toJSONString(context);
            throw new BizException(message);
        }
        return strategy;
    }
}
