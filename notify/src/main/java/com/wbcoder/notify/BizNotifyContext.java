package com.wbcoder.notify;

import com.wbcoder.notify.constans.AssertMsg;
import com.wbcoder.notify.constans.BizException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BizNotifyContext {
    /**
     * 通知类型
     */
    private String notifyType;
    /**
     * 范围
     */
    private String scope;
    /**
     * 标题
     */
    private String title;
    /**
     * 路径
     */
    private String path;
    /**
     * 请求参数
     */
    private Object requestParams;
    /**
     * 未知异常
     */
    private Exception unknownException;
    /**
     * 业务异常
     */
    private BizException bizException;

    public static BizNotifyContext defaultNew(String scope, String title, String path, Object requestParams,
                                              Exception e, BizException bizException) {
        return new BizNotifyContext(BizNotifyTypeEnum.WXG.getCode(), scope, title, path, requestParams, e, bizException);
    }

    public static BizNotifyContext biz(String title, String path, Object requestParams, BizException bizException) {
        return new BizNotifyContext(BizNotifyTypeEnum.WXG.getCode(), AssertMsg.INTERNAL,
                title, path, requestParams, null, bizException);
    }

    public static BizNotifyContext client(String title, String path, Object requestParams, Exception e) {
        return new BizNotifyContext(BizNotifyTypeEnum.WXG.getCode(), AssertMsg.EXTERNAL,
                title, path, requestParams, e, null);
    }

}
