package com.wbcoder.notify.constans;

public class BizException extends RuntimeException {

    public BizException() {
        super();
    }

    public BizException(String s) {
        super(s);
    }

    public BizException(String message, Throwable cause) {
        super(message, cause);
    }

    public BizException(Throwable cause) {
        super(cause);
    }
}
