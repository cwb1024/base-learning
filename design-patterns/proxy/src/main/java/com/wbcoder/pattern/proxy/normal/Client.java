package com.wbcoder.pattern.proxy.normal;

/**
  * @description: 测试类
  * @author: chengwb
  * @Date: 2019-09-19 01:10
  */
public class Client {

    public static void main(String[] args) {

        GamePlayer gamePlayer = new GamePlayer("Mr.Cheng");

        System.out.println("开始时间：" + "2019-09-18");

        gamePlayer.killBoss();

        gamePlayer.upgrade();

        System.out.println("结束时间：" + "2019-09-19");
    }
}
