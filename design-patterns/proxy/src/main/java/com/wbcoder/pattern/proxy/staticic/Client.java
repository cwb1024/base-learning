package com.wbcoder.pattern.proxy.staticic;

/**
  * @description: 测试类
  * @author: chengwb
  * @Date: 2019-09-19 01:22
  */
public class Client {

    public static void main(String[] args) {

        //玩家
        GamePlayer gamePlayer = new GamePlayer("Mr.Cheng");

        //找个代理
        GamePlayerProxy gamePlayerProxy = new GamePlayerProxy(gamePlayer);

        System.out.println("开始时间：" + "2019-09-18");

        //代理类执行方法，实现代理类调用原对象的方法，加一个这个结构有什么用？
        gamePlayerProxy.killBoss();

        gamePlayerProxy.upgrade();

        System.out.println("结束时间：" + "2019-09-19");
    }
}
