package com.wbcoder.pattern.proxy.staticic;


/**
  * @description: 定义一个游戏者
  * @author: chengwb
  * @Date: 2019-09-19 01:08
  */
public class GamePlayer implements IGamePlayer {

    private String gameUserName = "";

    public GamePlayer(String gameUserName) {
        this.gameUserName = gameUserName;
    }

    @Override
    public void login(String userName, String password) {
        System.out.println(userName + "登陆了LOL系统。" );
    }

    @Override
    public void killBoss() {
        System.out.println(gameUserName + " 在打怪！");
    }

    @Override
    public void upgrade() {
        System.out.println(gameUserName + "又升了 1 级！");
    }
}
