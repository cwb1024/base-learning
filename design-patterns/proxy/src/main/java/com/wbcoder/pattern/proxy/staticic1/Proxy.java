package com.wbcoder.pattern.proxy.staticic1;

public class Proxy implements ISubject {

    private ISubject subject = null;

    public Proxy() {
        this.subject = new Proxy();
    }

    public Proxy(Object... objects) {

    }

    @Override
    public void request() {
        this.before();
        this.subject.request();
        this.after();
    }

    /**
     * 代理对象 的一些执行前操作
     */
    public void before(){

    }

    /**
     * 代理对象 的 一些执行后操作
     */
    public void after(){

    }
}
