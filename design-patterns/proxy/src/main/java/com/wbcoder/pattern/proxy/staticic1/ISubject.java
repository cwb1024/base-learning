package com.wbcoder.pattern.proxy.staticic1;

public interface ISubject {

    public void request();
}
