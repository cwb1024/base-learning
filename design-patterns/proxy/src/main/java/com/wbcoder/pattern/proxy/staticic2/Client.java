package com.wbcoder.pattern.proxy.staticic2;

/**
  * @description: 普通代理测试类
  * @author: chengwb
  * @Date: 2019-09-19 01:53
  */
public class Client {

    public static void main(String[] args) {


        IGamePlayer gamePlayerProxy = new GamePlayerProxy("Mr.Cheng");

        System.out.println("开始时间：" + "2019-09-18");

        gamePlayerProxy.killBoss();

        gamePlayerProxy.upgrade();

        System.out.println("结束时间：" + "2019-09-19");
    }
}
