package com.wbcoder.pattern.proxy.dynamic;

public class SubjectImpl implements ISubject {
    @Override
    public String operate(String s) {

        System.out.println("subject is operating...");

        return "operate is ok!";
    }
}
