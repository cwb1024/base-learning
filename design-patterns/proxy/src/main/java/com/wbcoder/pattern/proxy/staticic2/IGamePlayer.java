package com.wbcoder.pattern.proxy.staticic2;

/**
  * @description: 定义一种规范
  * @author: chengwb
  * @Date: 2019-09-19 01:05
  */
public interface IGamePlayer {

    /**
     * 登录
     * @param userName
     * @param password
     */
    public void login(String userName, String password);


    /**
     * 杀怪
     */
    public void killBoss();

    /**
     * 升级
     */
    public void upgrade();


}
