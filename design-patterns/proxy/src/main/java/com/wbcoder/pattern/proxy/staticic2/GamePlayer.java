package com.wbcoder.pattern.proxy.staticic2;

/**
  * @description: 普通代理游戏者
  * @author: chengwb
  * @Date: 2019-09-19 01:45
  */

public class GamePlayer implements IGamePlayer {

    private String name = "";

    public GamePlayer(IGamePlayer gamePlayer, String name) throws Exception{
        if (gamePlayer == null) {
            throw new Exception("不能创建角色！！！");
        } else {
            this.name = name;
        }
    }

    @Override
    public void login(String userName, String password) {
        System.out.println(userName + "登陆了LOL系统。" );
    }

    @Override
    public void killBoss() {
        System.out.println(name + " 在打怪！");
    }

    @Override
    public void upgrade() {
        System.out.println(name + "又升了 1 级！");
    }
}
