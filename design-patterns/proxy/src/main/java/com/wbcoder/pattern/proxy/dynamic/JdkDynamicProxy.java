package com.wbcoder.pattern.proxy.dynamic;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * JDK动态代理利用的是JDK代理的特性，面向接口的，对目标接口进行增强
 */
public class JdkDynamicProxy implements InvocationHandler {

    //通过构造器初始化
    private ISubject target;

    public JdkDynamicProxy(ISubject target) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        System.out.println("动态代理执行之前...");

        Object res = method.invoke(target, args);

        System.out.println("动态代理执行之后...");

        return res;
    }

    /**
     * 生成动态代理对象
     *
     * @param target
     * @return
     */
    public static ISubject getProxy(ISubject target) {
        return (ISubject) Proxy.newProxyInstance(target.getClass().getClassLoader(), target.getClass().getInterfaces(), new JdkDynamicProxy(target));
    }
}
