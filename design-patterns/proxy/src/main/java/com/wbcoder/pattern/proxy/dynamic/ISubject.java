package com.wbcoder.pattern.proxy.dynamic;

public interface ISubject {

    String operate(String s);
}
