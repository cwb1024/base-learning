package com.wbcoder.pattern.proxy.staticic;

/**
 * 游戏玩家功能定义
 */
public interface IGamePlayer {

    /**
     * 登录
     * @param userName
     * @param password
     */
    public void login(String userName, String password);


    /**
     * 杀怪
     */
    public void killBoss();

    /**
     * 升级
     */
    public void upgrade();


}
