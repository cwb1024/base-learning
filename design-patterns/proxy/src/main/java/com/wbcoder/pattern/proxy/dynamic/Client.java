package com.wbcoder.pattern.proxy.dynamic;

public class Client {

    public static void main(String[] args) {

        SubjectImpl target = new SubjectImpl();

        ISubject proxy = JdkDynamicProxy.getProxy(target);

        proxy.operate("代理类执行逻辑,具体执行调用会调用到目标类的方法.");

    }
}
