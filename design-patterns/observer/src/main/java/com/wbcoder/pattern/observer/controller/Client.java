package com.wbcoder.pattern.observer.controller;

import com.wbcoder.pattern.observer.MessageEvent;
import com.wbcoder.pattern.observer.concreate.ConcreteObservable;
import com.wbcoder.pattern.observer.concreate.ConcreteObserverOne;
import com.wbcoder.pattern.observer.concreate.ConcreteObserverTwo;

public class Client {

    public static void main(String[] args) {

        long startTime = System.currentTimeMillis();

        //定义两个被观察者
        ConcreteObserverOne observerOne = new ConcreteObserverOne();
        ConcreteObserverTwo observerTwo = new ConcreteObserverTwo();

        //被观察者注册到观察者上
        ConcreteObservable concreteObservable = new ConcreteObservable();
        concreteObservable.register(observerOne);
        concreteObservable.register(observerTwo);

        //上面的依赖关系，在spring中启动的时候就注册好
        // ======================================


        //一个请求打过来的时候，派发事件，然后进行逻辑的执行
        MessageEvent messageEvent = new MessageEvent();

        //观察者通知被观察者事件发生了...[push模式]
        concreteObservable.notifyEvent(messageEvent);

        long endTime = System.currentTimeMillis();

        System.out.println(String.format("耗时: %s .", endTime - startTime));

    }
}
