package com.wbcoder.pattern.observer.inter;

import com.wbcoder.pattern.observer.MessageEvent;

/**
 * 被观察者(订阅者)，感知事件进行更新操作
 */
public interface Observer {

    void update(MessageEvent messageEvent);
}
