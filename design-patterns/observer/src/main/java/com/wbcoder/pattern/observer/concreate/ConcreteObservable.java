package com.wbcoder.pattern.observer.concreate;

import com.wbcoder.pattern.observer.MessageEvent;
import com.wbcoder.pattern.observer.inter.Observable;
import com.wbcoder.pattern.observer.inter.Observer;

import java.util.ArrayList;
import java.util.List;

/**
 * 具体的观察者
 */
public class ConcreteObservable implements Observable {

    private List<Observer> observers = new ArrayList<>();

    @Override
    public void register(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void remove(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyEvent(MessageEvent messageEvent) {
        for (Observer observer : observers) {
            observer.update(messageEvent);
        }
    }
}
