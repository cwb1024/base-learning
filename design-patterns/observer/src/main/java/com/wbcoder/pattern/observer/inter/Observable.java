package com.wbcoder.pattern.observer.inter;

import com.wbcoder.pattern.observer.MessageEvent;

/**
 * 观察者(支持订阅者｜被观察者注册、删除、事件通知功能)
 */
public interface Observable {

    void register(Observer observer);

    void remove(Observer observer);

    void notifyEvent(MessageEvent messageEvent);
}
