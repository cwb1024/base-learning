package com.wbcoder.pattern.observer.concreate;

import com.wbcoder.pattern.observer.MessageEvent;
import com.wbcoder.pattern.observer.inter.Observer;

/**
 * 具体的被观察者2
 */
public class ConcreteObserverTwo implements Observer {

    @Override
    public void update(MessageEvent messageEvent) {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(this.getClass().getName() + "get event and run...");
    }
}
