package com.wbcoder.pattern.decorator;

/**
 * 原本的类
 */
public class ConcreteComponent implements IComponent {

    private String name;

    public ConcreteComponent(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public double getSpend() {
        return 0;
    }
}
