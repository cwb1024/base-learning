package com.wbcoder.pattern.decorator;

/**
 * 装饰器类 注意它利用了组合的方式，同时注意函数实现的部分
 */
public class MilkDecorator implements IComponent {

    //对这个对象增强
    IComponent coffee;

    //构造初始化
    public MilkDecorator(IComponent coffee) {
        this.coffee = coffee;
    }

    //扩展的 ,牛奶 就是增强
    @Override
    public String getName() {
        return coffee.getName() + ", 牛奶";
    }

    //扩展的 +2D 就是增强
    @Override
    public double getSpend() {
        return coffee.getSpend() + 2D;
    }
}
