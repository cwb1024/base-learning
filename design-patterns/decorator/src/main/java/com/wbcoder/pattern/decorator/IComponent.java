package com.wbcoder.pattern.decorator;

/**
 * 定义功能接口
 */
public interface IComponent {

    String getName();

    double getSpend();
}
