package com.wbcoder.pattern.mediator;

/**
 * 抽象中介者:用来定义参与者与中介者之间的交互方式
 */
public interface Mediator {
    // 定义处理逻辑
    void doEvent(Colleague colleague);
}
