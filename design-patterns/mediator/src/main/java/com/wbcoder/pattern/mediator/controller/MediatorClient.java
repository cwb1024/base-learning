package com.wbcoder.pattern.mediator.controller;

import com.wbcoder.pattern.mediator.DispatchCenter;
import com.wbcoder.pattern.mediator.impl.MotorCarOneColleague;
import com.wbcoder.pattern.mediator.impl.MotorCarThreeColleague;
import com.wbcoder.pattern.mediator.impl.MotorCarTwoColleague;

import java.util.Arrays;

/**
 * 但是中介者的应用场景还是比较少见的，
 * 针对一些类依赖严重，形成的类似网状结构，改成一个类似与蒲公英一样结构，由中间向外扩散，来达到解耦合的效果。
 * <p>
 * 中介者和观察者模式很像
 */
public class MediatorClient {

    public static void main(String[] args) {

        MotorCarOneColleague oneColleague = new MotorCarOneColleague();
        MotorCarTwoColleague twoColleague = new MotorCarTwoColleague();
        MotorCarThreeColleague threeColleague = new MotorCarThreeColleague();

        //获取中介者，调度中心
        DispatchCenter dispatchCenter = new DispatchCenter(
                Arrays.asList(
                        oneColleague,
                        twoColleague,
                        threeColleague
                ));


        dispatchCenter.doEvent(oneColleague);
        //result -->
        //高铁二号收到消息！！！
        //高铁三号收到消息！！！

        dispatchCenter.doEvent(twoColleague);
        //result -->
        //高铁一号收到消息！！！
        //高铁三号收到消息！！！
    }
}
