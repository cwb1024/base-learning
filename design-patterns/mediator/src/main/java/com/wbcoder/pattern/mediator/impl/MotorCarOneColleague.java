package com.wbcoder.pattern.mediator.impl;

import com.wbcoder.pattern.mediator.Colleague;

/**
 * 具体的实现Colleague中的方法。
 */
public class MotorCarOneColleague implements Colleague {
    @Override
    public void message() {
        // 模拟处理业务逻辑
        System.out.println("高铁一号收到消息！！！");
    }
}
