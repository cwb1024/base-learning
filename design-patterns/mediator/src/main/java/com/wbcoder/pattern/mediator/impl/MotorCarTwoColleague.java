package com.wbcoder.pattern.mediator.impl;

import com.wbcoder.pattern.mediator.Colleague;

/**
 * 具体的实现Colleague中的方法。
 */
public class MotorCarTwoColleague implements Colleague {
    @Override
    public void message() {
        System.out.println("高铁二号收到消息！！！");
    }
}
