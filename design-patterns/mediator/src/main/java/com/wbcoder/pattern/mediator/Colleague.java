package com.wbcoder.pattern.mediator;

/**
 * 抽象同事角色:抽象类或者接口，主要用来定义参与者如何进行交互。
 */
//抽象参与者， 也可以使用abstract 写法
public interface Colleague {
    // 沟通消息
    void message();
}
