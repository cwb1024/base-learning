package com.wbcoder.pattern.mediator;

import java.util.List;

/**
 * 中介者实现：调度中心
 */
public class DispatchCenter implements Mediator {

    // 管理有哪些参与者
    private List<Colleague> colleagues;

    public DispatchCenter(List<Colleague> colleagues) {
        this.colleagues = colleagues;
    }

    @Override
    public void doEvent(Colleague colleague) {
        for (Colleague item : colleagues) {
            if (item == colleague) {
                // 如果是本身高铁信息，可以处理其他的业务逻辑
                // doSomeThing();
                continue;
            }
            // 通知其他参与
            item.message();
        }
    }
}
