package com.wbcoder.pattern.strategy_factory_map.controller;


import com.wbcoder.pattern.strategy_factory_map.IStrategy;
import com.wbcoder.pattern.strategy_factory_map.StrategyFactory;
import com.wbcoder.pattern.strategy_factory_map.impl.StrategyA;
import com.wbcoder.pattern.strategy_factory_map.impl.StrategyB;
import com.wbcoder.pattern.strategy_factory_map.impl.StrategyC;

public class StrategyClient {

    public static void main(String[] args) {
        String name = "";
        Object param = new Object();
        invokeStrategy(name, param);

        invokeStrategyFactory(name, param);
    }

    //选择策略调用
    private static void invokeStrategy(String name, Object param) {
        if (name.equals("A")) {
            new StrategyA().algorithmMethod(param);
        } else if (name.equals("B")) {
            new StrategyB().algorithmMethod(param);
        } else if (name.equals("C")) {
            new StrategyC().algorithmMethod(param);
        }
    }

    //策略+工厂 优化 if/else| switch
    private static void invokeStrategyFactory(String name, Object param) {
        IStrategy strategy = StrategyFactory.getStrategy(name);
        strategy.algorithmMethod(param);
    }
}
