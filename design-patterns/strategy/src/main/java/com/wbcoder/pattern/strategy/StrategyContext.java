package com.wbcoder.pattern.strategy;

/**
 * 策策略上下文，负责和具体的策略实现交互，通常策略上下文对象会持有一个真正的策略实现对象，
 * 策略上下文还可以让具体的策略实现从其中获取相关数据，回调策略上下文对象的方法。
 */
public class StrategyContext {

    /**
     * 策略实现的引用
     */
    private IStrategy strategy;

    public StrategyContext(IStrategy strategy) {
        this.strategy = strategy;
    }

    public void contextMethod(Object o) {
        // 调用策略实现的方法
        strategy.algorithmMethod(o);
    }
}
