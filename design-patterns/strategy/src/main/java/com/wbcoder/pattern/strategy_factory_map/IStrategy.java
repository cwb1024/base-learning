package com.wbcoder.pattern.strategy_factory_map;

public interface IStrategy {

    /**
     * 每一种策略需要实现的方法
     *
     * @param o
     */
    void algorithmMethod(Object o);
}
