package com.wbcoder.pattern.strategy_factory_map.impl;

import com.wbcoder.pattern.strategy_factory_map.StrategyFactory;
import com.wbcoder.pattern.strategy_factory_map.IStrategy;
import org.springframework.beans.factory.InitializingBean;

public class StrategyB implements IStrategy, InitializingBean {

    @Override
    public void algorithmMethod(Object o) {

    }

    @Override
    public void afterPropertiesSet() throws Exception {
        StrategyFactory.register(this.getClass().getName(),this);
    }
}
