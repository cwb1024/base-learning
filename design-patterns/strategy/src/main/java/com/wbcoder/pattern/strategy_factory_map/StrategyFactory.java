package com.wbcoder.pattern.strategy_factory_map;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 工厂+Map注册中心
 */
public class StrategyFactory {

    private static Map<String, IStrategy> strategyMap = new ConcurrentHashMap<>();

    public static IStrategy getStrategy(String name) {
        return strategyMap.get(name);
    }

    //createBean操作： 策略注入，这一步骤可以利用spring-InitializingBean特性启动注入
    public static void register(String name, IStrategy strategy) {
        strategyMap.put(name, strategy);
    }
}
