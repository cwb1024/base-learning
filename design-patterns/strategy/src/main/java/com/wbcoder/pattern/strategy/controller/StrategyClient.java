package com.wbcoder.pattern.strategy.controller;

import com.wbcoder.pattern.strategy.IStrategy;
import com.wbcoder.pattern.strategy.StrategyContext;
import com.wbcoder.pattern.strategy.impl.StrategyA;
import com.wbcoder.pattern.strategy.impl.StrategyB;
import com.wbcoder.pattern.strategy.impl.StrategyC;

public class StrategyClient {

    public static void main(String[] args) {
        String name = "";
        Object param = new Object();

        invokeStrategy(name, param);
    }

    private static void invokeStrategy(String name, Object param) {
        //选择并且创建策略
        IStrategy strategy = null;
        if (name.equals("A")) {
            strategy = new StrategyA();
        } else if (name.equals("B")) {
            strategy = new StrategyB();
        } else if (name.equals("C")) {
            strategy = new StrategyC();
        }

        //2. 在创建策略上下文的同时，将具体的策略实现对象注入到策略上下文当中
        StrategyContext context = new StrategyContext(strategy);

        //3. 调用上下文对象的方法来完成对具体策略实现的回调
        context.contextMethod(param);
    }
}
