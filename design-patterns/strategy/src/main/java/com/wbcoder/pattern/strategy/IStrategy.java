package com.wbcoder.pattern.strategy;

/**
 * 抽象策略接口
 */
public interface IStrategy {

    /**
     * 每一种策略需要实现的方法
     *
     * @param o
     */
    void algorithmMethod(Object o);
}
