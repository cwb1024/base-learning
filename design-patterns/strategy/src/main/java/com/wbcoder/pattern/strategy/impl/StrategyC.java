package com.wbcoder.pattern.strategy.impl;

import com.wbcoder.pattern.strategy.IStrategy;

public class StrategyC implements IStrategy {

    @Override
    public void algorithmMethod(Object o) {
        System.out.println(this.getClass().getName()+" is running...");
    }
}
