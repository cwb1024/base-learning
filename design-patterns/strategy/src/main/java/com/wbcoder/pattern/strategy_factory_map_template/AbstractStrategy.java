package com.wbcoder.pattern.strategy_factory_map_template;

import org.springframework.beans.factory.InitializingBean;

public abstract class AbstractStrategy implements InitializingBean, IStrategy {

    @Override
    public void commonMethod(Object o) {
        System.out.println("AbstractStrategy commonMethod .");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        StrategyFactory.register(this.getClass().getName(),this);
    }
}
