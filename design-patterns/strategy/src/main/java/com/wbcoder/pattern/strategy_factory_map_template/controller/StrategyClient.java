package com.wbcoder.pattern.strategy_factory_map_template.controller;


import com.wbcoder.pattern.strategy_factory_map_template.AbstractStrategy;
import com.wbcoder.pattern.strategy_factory_map_template.StrategyFactory;

public class StrategyClient {

    public static void main(String[] args) {
        String name = "";
        Object param = new Object();
        invokeStrategyFactory(name, param);
    }


    //策略+工厂+模板(抽象，减轻子类污染)
    private static void invokeStrategyFactory(String name, Object param) {
        AbstractStrategy strategy = StrategyFactory.getStrategy(name);
        strategy.algorithmMethod(param);
    }
}
