package com.wbcoder.pattern.chain;

/**
 * 将请求的发送和接收解耦，让多个接收对象都有机会处理这个请求。
 * 将这些接收对象串成一条链，并沿着这条链传递这个请求，直到链上的某个接收对象能够处理它为止。
 */
public abstract class Handler {

    protected Handler handler;

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    //传递请求
    public abstract void handleRequest(Integer times);

}
