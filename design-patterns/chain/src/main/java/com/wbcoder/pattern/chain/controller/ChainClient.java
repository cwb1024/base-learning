package com.wbcoder.pattern.chain.controller;

import com.wbcoder.pattern.chain.FirstInterview;
import com.wbcoder.pattern.chain.Handler;
import com.wbcoder.pattern.chain.SecondInterview;
import com.wbcoder.pattern.chain.ThreeInterview;

public class ChainClient {

    public static void main(String[] args) {
        Handler first = new FirstInterview();
        Handler second = new SecondInterview();
        Handler three = new ThreeInterview();
        //串连
        first.setHandler(second);
        second.setHandler(three);

        // 第一次面试
        first.handleRequest(1);
        System.out.println();
        // 第二次面试
        first.handleRequest(2);
        System.out.println();
        // 第三次面试
        first.handleRequest(3);
        System.out.println();
    }

}
