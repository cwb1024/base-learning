package com.wbcoder.pattern.facade.subsystem2;

public class CameraImpl implements Camera{
    @Override
    public void takePicture() {
        System.out.println("拍照片");
    }
}
