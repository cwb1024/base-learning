package com.wbcoder.pattern.facade.controller;

import com.wbcoder.pattern.facade.PhoneFacade;

public class PhoneClient {

    public static void main(String[] args) {
        PhoneFacade phone = new PhoneFacade();
        phone.dail();
        phone.takePicture();
    }
}
