package com.wbcoder.pattern.facade.subsystem1;

public class PhoneImpl implements Phone{
    @Override
    public void dail() {
        System.out.println("打电话");
    }

    @Override
    public void hangup() {
        System.out.println("挂电话");
    }
}
