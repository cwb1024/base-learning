package com.wbcoder.pattern.facade.subsystem2;

public interface Camera {
    //拍照片
    void takePicture();
}
