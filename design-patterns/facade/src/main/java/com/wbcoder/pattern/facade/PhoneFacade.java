package com.wbcoder.pattern.facade;

import com.wbcoder.pattern.facade.subsystem1.Phone;
import com.wbcoder.pattern.facade.subsystem1.PhoneImpl;
import com.wbcoder.pattern.facade.subsystem2.Camera;
import com.wbcoder.pattern.facade.subsystem2.CameraImpl;

/**
 * 外观模式
 * 定义：又叫门面模式，提供一个统一的接口，用来访问子系统中的一群接口
 * 外观模式定义了一个高层接口，让子系统更容易使用
 * 类型：结构型
 * <p>
 * <p>
 * 简化了调用过程，无须了解、深入子系统，防止带来风险
 * 减少系统依赖、松散耦合
 * 更好的划分访问层次
 * 符合迪米特法则，即最少知道原则
 */
public class PhoneFacade {

    private Phone mPhone = new PhoneImpl();
    private Camera mCamera = new CameraImpl();

    /**
     * 上层定义统一的API访问
     */
    public void dail() {
        mPhone.dail();
    }

    public void close() {
        mPhone.hangup();
    }

    public void takePicture() {
        mCamera.takePicture();
    }
}
