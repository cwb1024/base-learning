package com.wbcoder.pattern.facade.subsystem1;

public interface Phone{
    //打电话
    void dail();
    //挂电话
    void hangup();
}

